'use strict';
/*******************************************************************************************/
/********************************************************************************************
This file (app.js) has Express node module which will create web server for entire application
and Swagger is a API documentation and it's used to define an endpoints and parameters.

********************************************************************************************/
/*******************************************************************************************/

var SwaggerExpress = require('swagger-express-mw');
// var app = require('express')();
// var express = require('express');
var iniParserModule = require('parse-ini');
var cmd = require('node-cmd');
var exportedRequiredModules = require('./config.js').requiredModulesExport;
var copyFile = require('quickly-copy-file');
var jsforce = require('jsforce');
var app = exportedRequiredModules.appObj;
// 24 * 60 * 60 * 1000 => 24 hours
app.use(exportedRequiredModules.sessionObj({ secret: "secret", domain: 'localhost' , cookie: { maxAge: 24 * 60 * 60 * 1000 },saveUninitialized: true, resave: false}));
const {
  exec
} = require('child_process');
var configObj = exportedRequiredModules.iniParserModuleObj.parse('./config/LocalConfig.ini');

var port = ''
var localMachine = ''
  try {
    /* Gets the port in which the server is supposed to listen from "LocalConfig.ini" file */
    
    port = process.env.PORT || configObj.Local.port;
    localMachine = configObj.Local.machine;
    exportedRequiredModules.loggerObj.info("Port Running in " + port);
    console.log("Port Running in " + port);
  } catch (err) {
    /* If any error occurs , server will listen to the port 80  */
    port = process.env.PORT || 80;
    localMachine = "localhost"
    exportedRequiredModules.loggerObj.info("Port Running in " + port);
    console.log("Port Running in " + port);
  }
var config = {
  appRoot: __dirname
};
var salesforceDetails = exportedRequiredModules.credentialsObj.Salesforce;
var instanceUrl = "https://"+salesforceDetails.host

const oauth2 = new jsforce.OAuth2({
  loginUrl: instanceUrl,
  clientId: salesforceDetails.consumerKey,
  clientSecret: salesforceDetails.consumerSecret,
  redirectUri: salesforceDetails.callbackUrl
});


app.get("/mglenigan/auth/login", function(req, res) {
  // Redirect to Salesforce login/authorization page
  exportedRequiredModules.sessObj=req.session;

  if(exportedRequiredModules.sessObj[req.query.UserId] != undefined){
	  // console.log("code sessObj",exportedRequiredModules.sessObj);
	  var expireDate = exportedRequiredModules.moment(exportedRequiredModules.sessObj.cookie._expires,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
    // res.send({"UserToken" : exportedRequiredModules.sessObj[req.query.UserId], "Status" : "Old"});
	res.send({"UserId" : req.query.UserId, "UserToken" : exportedRequiredModules.sessObj[req.query.UserId], "expireDateTime" : expireDate, "Status" : "Old"});
  }else{
    var replaceURL = oauth2.getAuthorizationUrl();
    replaceURL = replaceURL.replace(instanceUrl, 'https://4cmeritdev-4cglenigan.cs85.force.com/NewGlenigan');
    // console.log("replaceURL",replaceURL)
    // res.send({"Message" : replaceURL});
    // res.redirect(oauth2.getAuthorizationUrl());
    res.redirect(replaceURL);
  }
});

app.get('/mglenigan/token', function(req, res) {
  exportedRequiredModules.sessObj=req.session;
  const conn = new jsforce.Connection({oauth2: oauth2});
      const code = req.query.code;
      // console.log("req",req);
      conn.authorize(req.query.code, function(err, userInfo) {
        // console.log("userInfo",userInfo)
        if (err) { return console.error("This error is in the auth callback: " + err); }
        // console.log('Access Token: ' + conn.accessToken);
        // console.log('Instance URL: ' + conn.instanceUrl);
        // console.log('refreshToken: ' + conn.refreshToken);
        // console.log('User ID: ' + userInfo.id);
        if(exportedRequiredModules.sessObj[userInfo.id] == undefined){
          exportedRequiredModules.sessObj[userInfo.id] = code
        }
        
        // console.log('Org ID: ' + userInfo.organizationId);
        // req.session.accessToken = conn.accessToken;
        // req.session.instanceUrl = conn.instanceUrl;
        // req.session.refreshToken = conn.refreshToken;
        const conn2 = new jsforce.Connection({
          instanceUrl : conn.instanceUrl,
          accessToken : conn.accessToken
        });
        conn2.identity(function(err, res) {
          if (err) { return console.error(err); }
          // console.log("user ID: " + res.user_id);
          // console.log("organization ID: " + res.organization_id);
          // console.log("username: " + res.username);
          // console.log("display name: " + res.display_name);
        });
        // console.log("code",req.hostname);
        // console.log("code sessObj",exportedRequiredModules.sessObj);
			// var expireDate = exportedRequiredModules.sessObj.cookie._expires;
			var expireDate = exportedRequiredModules.moment(exportedRequiredModules.sessObj.cookie._expires,'YYYY-MM-DDTHH:mm:ss.SSS[Z]').toISOString();
			// exportedRequiredModules.sessObj["expireDateTime"] = expireDate
           var string = encodeURIComponent('true');
           res.header('Authorization', exportedRequiredModules.sessObj[userInfo.id]);
		   if(localMachine.includes('') == true){
			   res.redirect(localMachine+'/mglenigan/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
		   }else{
		   res.redirect('http://'+localMachine+':'+port+'/mglenigan/valid?Id=' + userInfo.id+'&expireDateTime='+expireDate);
		   }
          
          // res.redirect('http://localhost:3000/sso/spinitsso-redirect');
  });

});

app.get("/mglenigan/valid", function(req, res) {
  // console.log(req.headers)
  exportedRequiredModules.sessObj=req.session;
  res.send({"UserId" : req.query.Id, "UserToken" : exportedRequiredModules.sessObj[req.query.Id], "expireDateTime" : req.query.expireDateTime, "Status" : "New"});
  // Redirect to Salesforce login/authorization page
  // res.send({"Message" : "Token generated"});
});
module.exports = app;
var sourcePath = exportedRequiredModules.credentialsObj.ESWrapperPath.path;

/******************************************************************************
SwaggerExpress is integrated with Swagger and Express Server
*******************************************************************************/
SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) {
    throw err;
  }
  process.on('uncaughtException', function (err) {
    exportedRequiredModules.loggerObj.error(err.stack);
  });
  // install middleware
  swaggerExpress.register(app);
  
  /* call the listen method to start the service */
  app.listen(port);

});