'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Tags.js) is used for fetching the response from
endpoint /glenigan/project/tags and endpoint /glenigan/comany/tags
Also it will create the document in ES.
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
    host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getProjectTags: getProjectTags,
    getOfficeTags: getOfficeTags,
    getContactTags: getContactTags,
    createOrUpdateProjectTags: createOrUpdateProjectTags,
    createOrUpdateOfficeTags: createOrUpdateOfficeTags,
    createOrUpdateContactTags: createOrUpdateContactTags,
    deleteProjectTags: deleteProjectTags,
    deleteOfficeTags: deleteOfficeTags,
    deleteContactTags: deleteContactTags
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the company endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
    exportedRequiredModules.loggerObj.error(error);
    response.status(errorCode);
    response.setHeader('Content-Type', 'application/json');
    response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: Tags
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getSizesQueryObj - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectTags(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            exportedRequiredModules.getProjectTagsQueryObj.getQueryByProjectTags(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticProjectTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {

                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });

            });

        }

    });




}

function getOfficeTags(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            exportedRequiredModules.getOfficeTagsQueryObj.getQueryByofficeTags(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticOfficeTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });

            });

        }

    });


}

function getContactTags(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            exportedRequiredModules.getContactTagsQueryObj.getQueryByContactTags(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticContactTagsTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {

                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });

            });

        }

    });



}

function createOrUpdateProjectTags(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var ProjectTagId = JSON.parse(request.body).TagId;
            if (ProjectTagId == undefined) {
                ProjectTagId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }

            var checkExistance;
            var elasticType = exportedRequiredModules.elasticProjectTagsTypeName;
            exportedRequiredModules.checkExistance(elasticServerClient, elasticType, ProjectTagId, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;
                    //     console.log("result",result)
                    // process.exit()
                    exportedRequiredModules.getProjectTagsQueryObj.postQueryByProjectTags(request, checkExistance, function (err, queryValue) {
                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentES(exportedRequiredModules, elasticServerClient, elasticType, ProjectTagId, queryValue, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentES(exportedRequiredModules, elasticServerClient, elasticType, ProjectTagId, queryValue, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });

        }

    });

}

function deleteProjectTags(request, response) {
    var ProjectTagId = request.query.TagId;

    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticProjectTagsTypeName;
            exportedRequiredModules.checkExistance(elasticServerClient, elasticType, ProjectTagId, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    if (checkExistance == true) {
                        exportedRequiredModules.deleteDocumentES(elasticServerClient, elasticType, ProjectTagId, function (error, result) {
                            if (error) {
                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                    else {
                        response.status(422);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify({ Error: "No Such Tags to Delete" }, null, 3));

                    }

                }
            });

        }

    });

}


function createOrUpdateContactTags(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var ContactTagId = JSON.parse(request.body).TagId;
            if (ContactTagId == undefined) {
                ContactTagId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticContactTagsTypeName;
            exportedRequiredModules.checkExistance(elasticServerClient, elasticType, ContactTagId, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;
                    // console.log("result",result)
                    // process.exit()
                    exportedRequiredModules.getContactTagsQueryObj.postQueryByContactTags(request, checkExistance, function (err, queryValue) {
                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentES(exportedRequiredModules, elasticServerClient, elasticType, ContactTagId, queryValue, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentES(exportedRequiredModules, elasticServerClient, elasticType, ContactTagId, queryValue, function (error, result) {
                                if (error) {
                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });

        }

    });


}

function deleteContactTags(request, response) {
    var ContactTagId = request.query.TagId;

    var checkExistance;
    var elasticType = exportedRequiredModules.elasticContactTagsTypeName;
    exportedRequiredModules.checkExistance(elasticServerClient, elasticType, ContactTagId, function (error, result) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
        }
        else {
            checkExistance = result;

            if (checkExistance == true) {
                exportedRequiredModules.deleteDocumentES(elasticServerClient, elasticType, ContactTagId, function (error, result) {
                    if (error) {
                        ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
            }
            else {
                response.status(422);
                response.setHeader('Content-Type', 'application/json');
                response.send(JSON.stringify({ Error: "No Such Tags to Delete" }, null, 3));

            }

        }
    });
}


function createOrUpdateOfficeTags(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
            var OfficeTagId = JSON.parse(request.body).TagId;
            if (OfficeTagId == undefined) {
                OfficeTagId = exportedRequiredModules.uuidObj(request.query.UserId + exportedRequiredModules.moment().toISOString());

            }
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticOfficeTagsTypeName;
            exportedRequiredModules.checkExistance(elasticServerClient, elasticType, OfficeTagId, function (error, result) {
                if (error) {
                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;
                    exportedRequiredModules.getOfficeTagsQueryObj.postQueryByOfficeTags(request, checkExistance, function (error, queryValue) {
                        if (checkExistance == true) {
                            exportedRequiredModules.updateDocumentES(exportedRequiredModules, elasticServerClient, elasticType, OfficeTagId, queryValue, function (error, result) {
                                if (error) {

                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else if (checkExistance == false) {
                            exportedRequiredModules.createDocumentES(exportedRequiredModules, elasticServerClient, elasticType, OfficeTagId, queryValue, function (error, result) {
                                if (error) {

                                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    response.status(200);
                                    response.setHeader('Content-Type', 'application/json');
                                    response.send(JSON.stringify(result, null, 3));
                                }

                            });

                        }
                    });
                }
            });

        }

    });


}



function deleteOfficeTags(request, response) {


    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {

            var OfficeTagId = request.query.TagId;
            //var postOfficeTags = require('../helpers/OfficeTagsQuery.js');
            var checkExistance;
            var elasticType = exportedRequiredModules.elasticOfficeTagsTypeName;
            exportedRequiredModules.checkExistance(elasticServerClient, elasticType, OfficeTagId, function (error, result) {
                if (error) {


                    ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                }
                else {
                    checkExistance = result;

                    if (checkExistance == true) {
                        exportedRequiredModules.deleteDocumentES(elasticServerClient, elasticType, OfficeTagId, function (error, result) {
                            if (error) {

                                ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                            }
                            else {
                                response.status(200);
                                response.setHeader('Content-Type', 'application/json');
                                response.send(JSON.stringify(result, null, 3));
                            }
                        });
                    }
                    else {
                        response.status(422);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));

                    }

                }
            });
        }

    });

}