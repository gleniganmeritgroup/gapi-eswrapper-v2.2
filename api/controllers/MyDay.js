'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (MyDay.js) is used for fetching the response from
endpoint /glenigan/myday/add

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	createMyDayEvent: createMyDayEvent,
	myDayCount: myDayCount,
	upcomingEvent: upcomingEvent,
	updateMyDayEvent: updateMyDayEvent,
	getMydayAllEvents: getMydayAllEvents,
	getMydayCalendarEvents: getMydayCalendarEvents,
	deleteEvent: deleteEvent
	
  };

/******************************************************************************
"ErrorResponse" function writes error occurring in the company endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: createMyDayEvent
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function createMyDayEvent (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.myDayTaskObj.createEvent(request, function(error, queryValue, myDayID){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					// request.myDayID = myDayID
					exportedRequiredModules.createDocumentES(exportedRequiredModules,elasticServerClient,elasticType,myDayID,queryValue, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}

/******************************************************************************
Function: deleteEvent
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function deleteEvent (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
	var userIDValue = request.query.UserId;
	var myDayID = request.query.MyDayID;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.deleteDocumentES(elasticServerClient,elasticType,myDayID, function(error, result){
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else{
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify(result, null, 3));
				}
			});
		}
	});
}

/******************************************************************************
Function: updateMyDayEvent
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function updateMyDayEvent (request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.myDayTaskObj.updateEvent(request, function(error, queryValue, myDayID){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.updateDocumentES(exportedRequiredModules,elasticServerClient,elasticType,myDayID,queryValue, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}


/******************************************************************************
Function: myDayCount
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function myDayCount(request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.myDayTaskObj.getCount(request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getRecordCount(exportedRequiredModules,elasticServerClient,elasticType,queryValue,request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else{
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}


/******************************************************************************
Function: upcomingEvent
Argument: request, response
Return: response

Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function upcomingEvent(request, response) {
	// res.setHeader('Content-Type', 'application/json');
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	// console.log("elasticType",elasticType)
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
			exportedRequiredModules.getMyDayEventObj.upcomingEvent(request, function(error, queryValue){
				
				if(error){
					ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForQueryException);
				}
				else
				{
					exportedRequiredModules.getMyDayEvents(exportedRequiredModules,elasticServerClient,elasticType,queryValue,request, function(error, result){
						if(error){
							ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(JSON.stringify(result, null, 3));
						}
					});
				}
			});
		}
	});
}

function getMydayAllEvents(request, response) {
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMyDayEventObj.getMydayAllEventsQuery(request, function (error, queryValue) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {
					var size = request.query.Size
					var queryValue = exportedRequiredModules.parserObj.parse(queryValue);
					if (typeof queryValue != 'object'){
						queryValue = JSON.parse(queryValue);
					}
					if (size != undefined) {
						queryValue.size = size;
					}
					exportedRequiredModules.getMyDayEvents(exportedRequiredModules, elasticServerClient, elasticType, queryValue, request, function (error, result){

						if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(result, null, 3);

						}
					});
				}
			});
		}
	});
}


function getMydayCalendarEvents(request, response) {
	var elasticType = exportedRequiredModules.elasticMyDayTypeName;
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getMyDayEventObj.getCalendarEventsQuery(request, function (error, queryValue) {

				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {
					var size = request.query.Size
					var queryValue = exportedRequiredModules.parserObj.parse(queryValue);
					if (typeof queryValue != 'object'){
						queryValue = JSON.parse(queryValue);
					}
					if (size != undefined) {
						queryValue.size = size;
					}
					exportedRequiredModules.getMyDayEvents(exportedRequiredModules, elasticServerClient, elasticType, queryValue, request, function (error, result){

						if (error) {
							ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
						}
						else {
							response.status(200);
							response.setHeader('Content-Type', 'application/json');
							response.send(result, null, 3);

						}
					});
				}
			});
		}
	});
}