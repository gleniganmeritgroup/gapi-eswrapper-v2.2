'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (Size.js) is used for fetching the response from
endpoint /glenigan/project/size

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/
var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
  host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
    getProjectFollow : getProjectFollow,
    getOfficeFollow : getOfficeFollow,
    createOrUpdateProjectFollow : createOrUpdateProjectFollow,
    createOrUpdateOfficeFollow : createOrUpdateOfficeFollow,
    deleteProjectFollow : deleteProjectFollow,
    deleteOfficeFollow : deleteOfficeFollow
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the company endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response,error, errorCode){
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}

/******************************************************************************
Function: size
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getSizesQueryObj - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response.
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function getProjectFollow (request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
		  
            exportedRequiredModules.getProjectFollowQueryObj.getQueryByProjectFollows(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
        
            });
		}
		
		});
  
    

}

function getOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            exportedRequiredModules.getOfficeFollowQueryObj.getQueryByOfficeFollow(request, function (err, queryValue) {

                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, queryValue, elasticType, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        //	console.log(res);
                        response.status(200);
                        response.setHeader('Content-Type', 'application/json');
                        response.send(JSON.stringify(result, null, 3));
                    }
                });
        
            });
		
		}
		
		});
  
}

function createOrUpdateProjectFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            var RequestParam = JSON.parse(request.body);
            var ProjectIds = RequestParam.ProjectId;
            var failureCount = 0 ;
            var successCount = 0 ;
            ProjectIds.split(',').forEach(function (ProjectId, index, array) {
                var ProjectFollowId = request.query.UserId + '#' + ProjectId;
                
            
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
                        exportedRequiredModules.getProjectFollowQueryObj.postQueryByProjectFollow(request, checkExistance, function (err, queryValue) {
                            if (checkExistance == true) {
                                exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectFollowId, ProjectId, queryValue, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else if (checkExistance == false) {
                                exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, ProjectFollowId, ProjectId, queryValue, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
    
                                });
    
                            }
                            if (index === array.length - 1){
                                response.status(200);
                               response.setHeader('Content-Type', 'application/json');
                               response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                           }
                        });
                    }
                });
            });	
		}
		
		});
	
   
}



function deleteProjectFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
            
            var ProjectIds = request.query.ProjectId;
            var failureCount = 0 ;
            var successCount = 0 ;
            ProjectIds.split(',').forEach(function (ProjectId, index, array) {
                
                var ProjectFollowId = request.query.UserId + '#' + ProjectId;
        
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticProjectFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);    
                    }
                    else {
                        checkExistance = result;
        
                        if (checkExistance == true) {
                            exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, ProjectFollowId, ProjectId, function (error, result) {
                                if (error) {
                                    failureCount++;
                                    // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    successCount++;
                                    // response.status(200);
                                    // response.setHeader('Content-Type', 'application/json');
                                    // response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else {
                            failureCount++;
        
                        }
                        if (index === array.length - 1){
                            response.status(200);
                           response.setHeader('Content-Type', 'application/json');
                           response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                       }
        
                    }
                });
            });
		
		}
		
		});
   
}

function createOrUpdateOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            var RequestParam = JSON.parse(request.body);
            var OfficeIds = RequestParam.OfficeId;
            var failureCount = 0 ;
            var successCount = 0 ;
            OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                var OfficeFollowId = request.query.UserId + '#' + OfficeId;
        
                
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
                        exportedRequiredModules.getOfficeFollowQueryObj.postQueryByOfficeFollow(request, checkExistance, function (error, queryValue) {
                            if (checkExistance == true) {
                                exportedRequiredModules.updateDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeFollowId, OfficeId, queryValue, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
                                });
                            }
                            else if (checkExistance == false) {
                                exportedRequiredModules.createDocumentESWithParent(exportedRequiredModules, elasticServerClient, elasticType, OfficeFollowId, OfficeId, queryValue, function (error, result) {
                                    if (error) {
                                        failureCount++;
                                        // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                    }
                                    else {
                                        successCount++;
                                        // response.status(200);
                                        // response.setHeader('Content-Type', 'application/json');
                                        // response.send(JSON.stringify(result, null, 3));
                                    }
        
                                });
        
                            }
                            if (index === array.length - 1){
                                response.status(200);
                               response.setHeader('Content-Type', 'application/json');
                               response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                           }
                        });
                    }
                });
            });
		}
		
		});
  
   

}



function deleteOfficeFollow(request, response) {
    exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request , function(error, subscriptionJSON,fieldsList, limits, request){
		if(error){
			ErrorResponse(response,error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else{
		
            
            var OfficeIds =  request.query.OfficeId;
            var failureCount = 0 ;
            var successCount = 0 ;
            OfficeIds.split(',').forEach(function (OfficeId, index, array) {
                var OfficeFollowId = request.query.UserId + '#' + OfficeId;
                var checkExistance;
                var elasticType = exportedRequiredModules.elasticOfficeFollowTypeName;
                exportedRequiredModules.checkExistanceWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId, function (error, result) {
                    if (error) {
                        ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                    }
                    else {
                        checkExistance = result;
        
                        if (checkExistance == true) {
                            exportedRequiredModules.deleteDocumentESWithParent(elasticServerClient, elasticType, OfficeFollowId, OfficeId, function (error, result) {
                                if (error) {
                                    failureCount++;
                                    // ErrorResponse(res, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
                                }
                                else {
                                    successCount++;
                                    // response.status(200);
                                    // response.setHeader('Content-Type', 'application/json');
                                    // response.send(JSON.stringify(result, null, 3));
                                }
                            });
                        }
                        else {
                            failureCount++;
        
                        }
                        if (index === array.length - 1){
                            response.status(200);
                           response.setHeader('Content-Type', 'application/json');
                           response.send(JSON.stringify({"Message" : "Success"}, null, 3));
                       }
        
                    }
                });
            });	
		}
		
		});
  
}