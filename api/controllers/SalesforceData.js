'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (County.js) is used for fetching the response from
endpoint /glenigan/project/county

********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Below line has the list of all required modules imported from config.js and 	
assigned to an variable ( == > exportedRequiredModules)

Connecting Elastic server using elasticsearch module (elasticsearchObj) and is assigned to an
object "elasticServerClient"
*******************************************************************************/

var exportedRequiredModules = require('../../config.js').requiredModulesExport;

var elasticServerClient = new exportedRequiredModules.elasticsearchObj.Client({
	host: exportedRequiredModules.elasticIndexHost
});

/******************************************************************************
"module.exports" to make this controller object visible to the rest of the 
program when we call it from Express Server in app.js
*******************************************************************************/
module.exports = {
	projectSavedSearch: projectSavedSearch,
	officeSavedSearch: officeSavedSearch,
	contactSavedSearch: contactSavedSearch,
	executequery: executequery,
	createSavedSearch:createSavedSearch,
	UpdateSavedSearch:UpdateSavedSearch,
	deleteSavedSearches:deleteSavedSearches,
	getExportOptionData:getExportOptionData
};

/******************************************************************************
"ErrorResponse" function writes error occurring in the company endpoint to 
EsWrapper.log and returns error response.
*******************************************************************************/
function ErrorResponse(response, error, errorCode) {
	exportedRequiredModules.loggerObj.error(error);
	response.status(errorCode);
	response.setHeader('Content-Type', 'application/json');
	response.send(JSON.stringify(error, null, 3));
}


/******************************************************************************
Function: county
Argument: request, response
Return: response
Usage:
	1. paramsValidationAndGetSubscriptionObj - used to validate requested
		parameters and get the subscription JSON.
	2. getLocationsQueryObj - used to form a elastic query based on user request.
	3. getResultObj - used to search the query into live elastic index.
	4. return the response
Exception function:
  	1. The function throwing exception, ErrorResponse will returns the exception. 
*******************************************************************************/
function projectSavedSearch(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getSalesforceQueryObj.projectSavedSearchDataList(request, function (error, result) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					//	resut=JSON.parse(result);
					//console.log(result);
					response.send(result, null, 3);
				}


			});
		}
	});
}

function officeSavedSearch(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getSalesforceQueryObj.officeSavedSearchDataList(request, function (error, result) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					//	resut=JSON.parse(result);
					//console.log(result);
					response.send(result, null, 3);
				}


			});
		}
	});
}

function contactSavedSearch(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getSalesforceQueryObj.contactSavedSearchDataList(request, function (error, result) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					//	resut=JSON.parse(result);
					//console.log(result);
					response.send(result, null, 3);
				}


			});
		}
	});
}
function executequery(request, response) {

	var userId = request.query.UserId;
	var SearchId = request.query.SearchId;
	exportedRequiredModules.getSalesforceQueryObj.searchQuery(userId, SearchId, function (error, SavedSearchData) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
		}
		else {
			// console.log("Salesforce",SavedSearchData[0].Field3__c);

			//var searchQuery = SavedSearchData[0].Field3__c;
			var searchQuery = SavedSearchData;
		//	console.log(searchQuery);
			var elasticType = exportedRequiredModules.elasticTypeName;

			exportedRequiredModules.getResultNotesTagFollow(exportedRequiredModules, elasticServerClient, searchQuery, elasticType, function (error, result) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {
					//	console.log(res);
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(JSON.stringify(result, null, 3));
				}
			});
			//console.log(searchQuery);

		}
		//res.end(JSON.stringify(SavedSearchData));
	});
}

function createSavedSearch(request, response) {
	var opType='Create';
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
			exportedRequiredModules.getSalesforceQueryObj.CUDSavedSearch(request,opType, function (error, successMessage) {
	
				if(error){
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else{
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(successMessage, null, 3);

				}
			});
		}
	});
	
	
}


function UpdateSavedSearch(request, response) {
	var opType='Update';
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
			exportedRequiredModules.getSalesforceQueryObj.CUDSavedSearch(request,opType, function (error, successMessage) {
	
				if(error){
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else{
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(successMessage, null, 3);

				}
			});
		}
	});
	
}


function deleteSavedSearches(request, response) {
	var opType='Delete';
	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
        if (error) {
            ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
        }
        else {
			exportedRequiredModules.getSalesforceQueryObj.CUDSavedSearch(request,opType, function (error, successMessage) {
	
				if(error){
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else{
					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					response.send(successMessage, null, 3);

				}
			});
		}
	});	
}


function getExportOptionData(request, response) {

	exportedRequiredModules.paramsValidationAndGetSubscriptionObj(request, function (error, subscriptionJSON, fieldsList, limits, request) {
		if (error) {
			ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForValidation);
		}
		else {
			exportedRequiredModules.getSalesforceQueryObj.ExportOptionList(request, function (error, result) {
				if (error) {
					ErrorResponse(response, error, exportedRequiredModules.errorCodeObj.codeForResponseException);
				}
				else {

					response.status(200);
					response.setHeader('Content-Type', 'application/json');
					//	resut=JSON.parse(result);
					//console.log(result);
					response.send(result, null, 3);
				}


			});
		}
	});
}