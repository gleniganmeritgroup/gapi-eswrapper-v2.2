'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactTagsLinkQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/
exports.getQueryByContactTagsLink = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var ContactTagId = requireQuery.query.ContactTagId;

	var ContactTagIds = [];

	if (ContactTagId != undefined) {
		ContactTagId.split(',').forEach(function (TagId) {

			ContactTagIds.push(TagId);
		});
	}
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userID Values  is Missing" }, null);
	}
	else {

		try {
			var ContactTagIdQuery = '';
			var queryBuildContactTagIds = {};
			queryBuildContactTagIds.ContactTagId = ContactTagIds;
			var queryBuildContactTagIdsField = '{"terms": %s}';
			if (ContactTagId != undefined) {
				var ContactTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildContactTagIdsField, JSON.stringify(queryBuildContactTagIds));
				var ContactTagsLink = '{"bool": {"must":[{"term":{ "ContactTagLinkTaggedByUserId" : "' + userIDValues + '"}},' + ContactTagIdQuery + '] }}';
				var FinalQuery = '{"query": ' + ContactTagsLink + '}';
				callback(null, FinalQuery);
			}else{

				var ContactTagsLink = '{"bool": {"must":[{"term":{ "ContactTagLinkTaggedByUserId" : "' + userIDValues + '"}}] }}';
				var FinalQuery = '{"query": ' + ContactTagsLink + '}';
				callback(null, FinalQuery);	
			}
		} catch (err) {
			callback(err, null);
		}
	}
}

exports.postQueryByContactTagLink = function (requireQuery,TagId, checkExistance, callback) {

	
	
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ContactTagLinkTaggedByUserId = requireQuery.query.UserId;
	var ContactTagId = TagId;
	var ContactTagLinkName = req.ContactId;


	var ContactTagLinkUserIdQuery = '';
	var ContactTagLinkTypeTagId = '';
	try {

		if (ContactTagLinkTaggedByUserId != undefined) { ContactTagLinkUserIdQuery = '"ContactTagLinkTaggedByUserId": "' + ContactTagLinkTaggedByUserId + '"'; }
		if (ContactTagId != undefined) { ContactTagLinkTypeTagId = ',"ContactTagId": "' + ContactTagId + '"'; }
		var ContactTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"ContactTagLinkCreatedDate": "' + ContactTagLinkCreatedDate + '"';

	


		var FQuery = ContactTagLinkUserIdQuery + ContactTagLinkTypeTagId + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
