'use strict';

exports.getQueryByProjectNotes = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var projectId= requireQuery.query.ProjectId
	
	if ((userIDValues == undefined)) {
		callback({Error:"Parameter userIDValues is Missing"},null);
	}
	else{
		
		try{
			if ((projectId == undefined)) {
				var ProjectNotes = '{"bool": {"must":[{"term":{ "ProjectNoteCreatedByUserId" : "'+userIDValues+'"}}] }}'; 
				var FinalQuery='{"query": '+ProjectNotes+'}';
				callback(null, FinalQuery);
			}else{
				var ProjectNotes = '{"bool": {"must":[{"term":{ "ProjectNoteCreatedByUserId" : "'+userIDValues+'"}},{"term":{ "ProjectNoteProjectId" : "'+projectId+'"}}] }}'; 
				var FinalQuery='{"query": '+ProjectNotes+'}';
				callback(null, FinalQuery);
			}

			
		}catch (err){
			callback(err, null);
		}
	}
}
exports.postQueryByProjectNotes = function (requireQuery, checkExistance, callback) {


	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ProjectNoteUserId = requireQuery.query.UserId;
	var ProjectNoteText = req.NoteText;
	var ProjectNoteProjectId = req.ProjectId;
	var ProjectNoteCompanyId =  requireQuery.AccountId;
	
	//var ProjectNoteUserIdQuery = '';
	var ProjectNoteTextQuery = '';
	var ProjectNoteProjectIdQuery = '';
	var ProjectNoteCompanyIdQuery = '';
	
	try {

		//if (ProjectNoteUserId != undefined) { ProjectNoteUserIdQuery = '"ProjectNoteCreatedByUserId": "' + ProjectNoteUserId + '"'; }
		if (ProjectNoteText != undefined) { ProjectNoteTextQuery = ',"ProjectNoteText": "' + ProjectNoteText + '"'; }
		if (ProjectNoteProjectId != undefined) { ProjectNoteProjectIdQuery = ',"ProjectNoteProjectId": "' + ProjectNoteProjectId + '"'; }
		if (ProjectNoteCompanyId != undefined) { ProjectNoteCompanyIdQuery = ',"ProjectNoteCompanyId": "' + ProjectNoteCompanyId + '"'; }
	

		if (checkExistance != false) {
			var ProjectNoteModifiedDateTime = datetime.toISOString();
			var ProjectNoteModifiedByUserId= ',"ProjectNoteModifiedByUserId": "' + ProjectNoteUserId + '"';
			var DateQuery = ProjectNoteModifiedByUserId+',"ProjectNoteModifiedDateTime": "' + ProjectNoteModifiedDateTime + '"';
		
		} else {
			//var ProjectNoteModifiedDateTime = datetime.toISOString();

		
			var ProjectNoteCreatedDateTime = datetime.toISOString();
			var ProjectNoteCreatedByUserId= ',"ProjectNoteCreatedByUserId": "' + ProjectNoteUserId + '"';
			var DateQuery = ProjectNoteCreatedByUserId+',"ProjectNoteCreatedDateTime": "' + ProjectNoteCreatedDateTime + '"';
		
		
		}




		var FQuery = ProjectNoteTextQuery + ProjectNoteProjectIdQuery + DateQuery+ProjectNoteCompanyIdQuery ;
		
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"ProjectNoteUserId": "' + ProjectNoteUserId + '","ProjectNoteType": "' + ProjectNoteType + '","ProjectNoteName": "' + ProjectNoteName + '","ProjectNoteCompanyId": "' + ProjectNoteCompanyId + '","ProjectNoteColour": "' + ProjectNoteColour + '"' + DateQuery + '}';
	//	console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
