'use strict';
exports.getQueryByProjectTagsLink = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var ProjectTagId = requireQuery.query.ProjectTagId;

	var ProjectTagIds = [];

	if (ProjectTagId != undefined) {
		ProjectTagId.split(',').forEach(function (TagId) {

			ProjectTagIds.push(TagId);
		});
	}
	try {
		var ProjectTagIdQuery = '';
		var queryBuildProjectTagIds = {};
		queryBuildProjectTagIds.ProjectTagId = ProjectTagIds;
		var queryBuildProjectTagIdsField = '{"terms": %s}';
		if (ProjectTagId != undefined) {

		
			var ProjectTagIdQuery =  exportedRequiredModules.convertComma2Array(queryBuildProjectTagIdsField, JSON.stringify(queryBuildProjectTagIds));
			var ProjectTagsLink = '{"bool": {"must":[{"term":{ "ProjectTagLinkTaggedByUserId" : "' + userIDValues + '"}},' + ProjectTagIdQuery + '] }}';
		}else{

		var ProjectTagsLink = '{"bool": {"must":[{"term":{ "ProjectTagLinkTaggedByUserId" : "' + userIDValues + '"}}] }}';
		}
		var FinalQuery = '{"query": ' + ProjectTagsLink + '}';
		//console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}

exports.postQueryByProjectTagLink = function (requireQuery,TagId, checkExistance, callback) {

	
	
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ProjectTagLinkTaggedByUserId =  requireQuery.query.UserId;
	var ProjectTagId = TagId;
	var ProjectTagLinkName = req.ProjectId;


	var ProjectTagLinkUserIdQuery = '';
	var ProjectTagLinkTypeTagId = '';
	try {

		if (ProjectTagLinkTaggedByUserId != undefined) { ProjectTagLinkUserIdQuery = '"ProjectTagLinkTaggedByUserId": "' + ProjectTagLinkTaggedByUserId + '"'; }
		if (ProjectTagId != undefined) { ProjectTagLinkTypeTagId = ',"ProjectTagId": "' + ProjectTagId + '"'; }
		var ProjectTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"ProjectTagLinkCreatedDate": "' + ProjectTagLinkCreatedDate + '"';

	


		var FQuery = ProjectTagLinkUserIdQuery + ProjectTagLinkTypeTagId + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"ProjectTagLinkUserId": "' + ProjectTagLinkUserId + '","ProjectTagLinkType": "' + ProjectTagLinkType + '","ProjectTagLinkName": "' + ProjectTagLinkName + '","ProjectTagLinkCompanyId": "' + ProjectTagLinkCompanyId + '","ProjectTagLinkColour": "' + ProjectTagLinkColour + '"' + DateQuery + '}';
		// console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
