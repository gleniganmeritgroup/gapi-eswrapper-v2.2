'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactNotesQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/


exports.getQueryByContactNotes = function (requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var ContactId=requireQuery.query.ContactId;
	
	if ((userIDValues == undefined)) {
		callback({Error:"Parameter userIDValues is Missing"},null);
	}
	else{
		
		try{
			if(ContactId == undefined){
				var ContactNotes = '{"bool": {"must":[{"term":{ "ContactNoteCreatedByUserId" : "'+userIDValues+'"}}] }}'; 
				var FinalQuery='{"query": '+ContactNotes+'}';
				callback(null, FinalQuery);
			}else{
				var ContactNotes = '{"bool": {"must":[{"term":{ "ContactNoteCreatedByUserId" : "'+userIDValues+'"}},{"term":{ "ContactNoteContactId" : "'+ContactId+'"}}] }}'; 
				var FinalQuery='{"query": '+ContactNotes+'}';
				callback(null, FinalQuery);
			}
			
		}catch (err){
			callback(err, null);
		}
	}
}

exports.postQueryByContactNotes = function (requireQuery, checkExistance, callback) {


	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var ContactNoteUserId = requireQuery.query.UserId;
	var ContactNoteText = req.NoteText;
	var ContactNoteContactId = req.ContactId;
	var ContactNoteCompanyId = requireQuery.AccountId;
	
	//var ContactNoteUserIdQuery = '';
	var ContactNoteTextQuery = '';
	var ContactNoteContactIdQuery = '';
	var ContactNoteCompanyIdQuery = '';
	
	try {

		//if (ContactNoteUserId != undefined) { ContactNoteUserIdQuery = '"ContactNoteCreatedByUserId": "' + ContactNoteUserId + '"'; }
		if (ContactNoteText != undefined) { ContactNoteTextQuery = ',"ContactNoteText": "' + ContactNoteText + '"'; }
		if (ContactNoteContactId != undefined) { ContactNoteContactIdQuery = ',"ContactNoteContactId": "' + ContactNoteContactId + '"'; }
		if (ContactNoteCompanyId != undefined) { ContactNoteCompanyIdQuery = ',"ContactNoteCompanyId": "' + ContactNoteCompanyId + '"'; }
	

		if (checkExistance != false) {
			var ContactNoteModifiedDateTime = datetime.toISOString();
			var ContactNoteModifiedByUserId= ',"ContactNoteModifiedByUserId": "' + ContactNoteUserId + '"';
			var DateQuery = ContactNoteModifiedByUserId+',"ContactNoteModifiedDateTime": "' + ContactNoteModifiedDateTime + '"';
		
		} else {
			//var ContactNoteModifiedDateTime = datetime.toISOString();

		
			var ContactNoteCreatedDateTime = datetime.toISOString();
			var ContactNoteCreatedByUserId= ',"ContactNoteCreatedByUserId": "' + ContactNoteUserId + '"';
			var DateQuery = ContactNoteCreatedByUserId+',"ContactNoteCreatedDateTime": "' + ContactNoteCreatedDateTime + '"';
		
		
		}

		var FQuery = ContactNoteTextQuery + ContactNoteContactIdQuery + DateQuery+ContactNoteCompanyIdQuery ;
		
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
