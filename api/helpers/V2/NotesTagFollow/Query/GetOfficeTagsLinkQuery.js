'use strict';
exports.getQueryByOfficeTagsLink = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var OfficeTagId = requireQuery.query.OfficeTagId;
	var OfficeTagIds = [];

	if (OfficeTagId != undefined) {
		OfficeTagId.split(',').forEach(function (offTagId) {

			OfficeTagIds.push(offTagId);
		});
	}
	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			var officeTagIdQuery = '';
			var queryBuildOfficeTagIds = {};
			queryBuildOfficeTagIds.OfficeTagId = OfficeTagIds;
			var queryBuildOfficeTagIdsField = '{"terms": %s}';


			if (OfficeTagId != undefined) {

				var officeTagIdQuery = exportedRequiredModules.convertComma2Array(queryBuildOfficeTagIdsField, JSON.stringify(queryBuildOfficeTagIds));
				var OfficeTagsLink = '{"bool": {"must":[{"term":{ "OfficeTagLinkTaggedByUserId" : "' + userIDValues + '"}},' + officeTagIdQuery + '] }}';

			}else{

				var OfficeTagsLink = '{"bool": {"must":[{"term":{ "OfficeTagLinkTaggedByUserId" : "' + userIDValues + '"}}] }}';
			}

		
			var FinalQuery = '{"query": ' + OfficeTagsLink + '}';
			//console.log("FinalQuery "+FinalQuery);
			callback(null, FinalQuery);
		} catch (err) {
			callback(err, null);
		}
	}
}


exports.postQueryByOfficeTagsLink = function (requireQuery,TagId, checkExistance, callback) {

	
	
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var OfficeTagLinkTaggedByUserId =  requireQuery.query.UserId;
	var OfficeTagId = TagId;
	


	var OfficeTagsLinkUserIdQuery = '';
	var OfficeTagsLinkTagIdQuery = '';
	try {

		if (OfficeTagLinkTaggedByUserId != undefined) { OfficeTagsLinkUserIdQuery = '"OfficeTagLinkTaggedByUserId": "' + OfficeTagLinkTaggedByUserId + '"'; }
		if (OfficeTagId != undefined) { OfficeTagsLinkTagIdQuery = ',"OfficeTagId": "' + OfficeTagId + '"'; }
		var OfficeTagLinkCreatedDate = datetime.toISOString();
		var DateQuery = ',"OfficeTagLinkCreatedDate": "' + OfficeTagLinkCreatedDate + '"';

	


		var FQuery = OfficeTagsLinkUserIdQuery + OfficeTagsLinkTagIdQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"OfficeTagsLinkUserId": "' + OfficeTagsLinkUserId + '","OfficeTagsLinkType": "' + OfficeTagsLinkType + '","OfficeTagsLinkName": "' + OfficeTagsLinkName + '","OfficeTagsLinkCompanyId": "' + OfficeTagsLinkCompanyId + '","OfficeTagsLinkColour": "' + OfficeTagsLinkColour + '"' + DateQuery + '}';
		//console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}