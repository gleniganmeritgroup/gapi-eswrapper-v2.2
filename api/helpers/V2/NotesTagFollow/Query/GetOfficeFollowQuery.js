'use strict';
exports.getQueryByOfficeFollow = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;


	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			var OfficeFolloa = '{"bool": {"must":[{"term":{ "OfficeFollowUserId" : "' + userIDValues + '"}}] }}';
			var FinalQuery = '{"query": ' + OfficeFolloa + '}';
			//console.log("FinalQuery "+FinalQuery);
			callback(null, FinalQuery);
		} catch (err) {
			callback(err, null);
		}
	}
}


exports.postQueryByOfficeFollow = function (requireQuery, checkExistance, callback) {


	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var OfficeFollowUserId = requireQuery.query.UserId;
	var OfficeFollowOfficeId = req.OfficeId;
	var OfficeFollowNotificationType = req.NotificationType;
	var OfficeFollowRelationship = req.Relationship;
	var OfficeFollowAlertSent = req.OfficeFollowAlertNote;
	var OfficeFollowAlertNote = req.OfficeFollowAlertNote;
	var OfficeFollowAlertDate = req.OfficeFollowAlertDate;
	var OfficeFollowUserIdQuery = ''; var OfficeFollowNotificationTypeQuery = ''; var OfficeFollowRelationshipQuery = '';
	var OfficeFollowAlertSentQuery = '';var OfficeFollowAlertNoteQuery = '';var OfficeFollowAlertDateQuery = '';
	try {

		if (OfficeFollowUserId != undefined) { OfficeFollowUserIdQuery = '"OfficeFollowUserId": "' + OfficeFollowUserId + '"'; }

		if (OfficeFollowNotificationType != undefined) { OfficeFollowNotificationTypeQuery = ',"OfficeFollowNotificationType": "' + OfficeFollowNotificationType + '"'; }
		if (OfficeFollowRelationship != undefined) { OfficeFollowRelationshipQuery = ',"Relationship": "' + OfficeFollowRelationship + '"'; }
		if (OfficeFollowAlertSent != undefined) { OfficeFollowAlertSentQuery = ',"OfficeFollowAlertSent": ' + JSON.stringify(OfficeFollowAlertSent); }
		if (OfficeFollowAlertNote != undefined) { OfficeFollowAlertNoteQuery = ',"OfficeFollowAlertNote": ' + JSON.stringify(OfficeFollowAlertNote); }
		if (OfficeFollowAlertDate != undefined) { OfficeFollowAlertDateQuery = ',"OfficeFollowAlertDate": ' + JSON.stringify(OfficeFollowAlertDate); }
		if (checkExistance != true) {
			var OfficeFollowCreatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeFollowCreatedDate": "' + OfficeFollowCreatedDate + '"';
			DateQuery = DateQuery + ',"LastProcessedDateTime": "' + OfficeFollowCreatedDate + '"';
		} else {

			var OfficeFollowUpdatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeFollowUpdatedDate": "' + OfficeFollowUpdatedDate + '"';
			DateQuery = DateQuery + ',"LastProcessedDateTime": "' + OfficeFollowUpdatedDate + '"';


		}



		// var FQuery = OfficeFollowUserIdQuery + OfficeFollowNotificationTypeQuery + OfficeFollowRelationshipQuery + ProjectFollowAlertSentQuery+ProjectFollowAlertDateQuery+ProjectFollowAlertNoteQuery +  DateQuery;
		var FQuery = OfficeFollowUserIdQuery + OfficeFollowNotificationTypeQuery + OfficeFollowRelationshipQuery + OfficeFollowAlertSentQuery+OfficeFollowAlertDateQuery+OfficeFollowAlertNoteQuery +  DateQuery;

		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}
		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"OfficeFollowUserId": "' + OfficeFollowUserId + '","OfficeFollowType": "' + OfficeFollowType + '","OfficeFollowName": "' + OfficeFollowName + '","OfficeFollowCompanyId": "' + OfficeFollowCompanyId + '","OfficeFollowColour": "' + OfficeFollowColour + '"' + DateQuery + '}';
		//console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
