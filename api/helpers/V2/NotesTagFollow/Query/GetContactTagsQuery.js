'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContactTagsQuery.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/
exports.getQueryByContactTags = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;


	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues  is Missing" }, null);
	}
	else {

		try {
			var ContactTag = '{"bool": {"must":[{"term":{ "ContactTagUserId" : "' + userIDValues + '"}}] }}';
			var FinalQuery = '{"query": ' + ContactTag + '}';
			// console.log("FinalQuery "+FinalQuery);
			callback(null, FinalQuery);
		} catch (err) {
			callback(err, null);
		}
	}
}
exports.postQueryByContactTags = function (requireQuery, checkExistance, callback) {

	var datetime = new Date();

	var req = JSON.parse(requireQuery.body);

	var ContactTagUserId = requireQuery.query.UserId;
	var ContactTagType = req.TagType;
	var ContactTagName = req.TagName;
	var ContactTagCompanyId = requireQuery.AccountId;
	var ContactTagColour = req.TagColour;

	var ContactTagUserIdQuery = '';
	var ContactTagTypeQuery = '';
	var ContactTagNameQuery = '';
	var ContactTagCompanyIdQuery = '';
	var ContactTagColourQuery = '';
	try {

		if (ContactTagUserId != undefined) { ContactTagUserIdQuery = '"ContactTagUserId": "' + ContactTagUserId + '"'; }
		if (ContactTagType != undefined) { ContactTagTypeQuery = ',"ContactTagType": "' + ContactTagType + '"'; }
		if (ContactTagName != undefined) { ContactTagNameQuery = ',"ContactTagName": "' + ContactTagName + '"'; }
		if (ContactTagCompanyId != undefined) { ContactTagCompanyIdQuery = ',"ContactTagCompanyId": "' + ContactTagCompanyId + '"'; }
		if (ContactTagColour != undefined) { ContactTagColourQuery = ',"ContactTagColour": "' + ContactTagColour + '"'; }

		if (checkExistance != false) {
			var ContactTagUpdatedDate = datetime.toISOString();
			var DateQuery = ',"ContactTagUpdatedDate": "' + ContactTagUpdatedDate + '"';

		} else {
			var ContactTagUpdatedDate = datetime.toISOString();
			var ContactTagCreatedDate = datetime.toISOString();
			var DateQuery = ',"ContactTagCreatedDate": "' + ContactTagCreatedDate + '","ContactTagUpdatedDate": "' + ContactTagUpdatedDate + '"';
		}

		var FQuery = ContactTagUserIdQuery + ContactTagTypeQuery + ContactTagNameQuery + ContactTagCompanyIdQuery + ContactTagColourQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}

		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
