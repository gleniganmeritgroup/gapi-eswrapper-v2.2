'use strict';
exports.getQueryByofficeTags = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;


	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userID Values  is Missing" }, null);
	}
	else {

		try {
			var OfficeTag = '{"bool": {"must":[{"term":{ "OfficeTagUserId" : "' + userIDValues + '"}}] }}';
			var FinalQuery = '{"query": ' + OfficeTag + '}';
			//console.log("FinalQuery "+FinalQuery);
			callback(null, FinalQuery);
		} catch (err) {
			callback(err, null);
		}
	}
}

exports.postQueryByOfficeTags = function (requireQuery, checkExistance, callback) {


	
	var datetime = new Date();

	var req = JSON.parse(requireQuery.body);
	var OfficeTagUserId = requireQuery.query.UserId;
	var OfficeTagType = req.TagType;
	var OfficeTagName = req.TagName;
	var OfficeTagCompanyId =requireQuery.AccountId;
	var OfficeTagColour = req.TagColour;

	var OfficeTagUserIdQuery = '';
	var OfficeTagTypeQuery = '';
	var OfficeTagNameQuery = '';
	var OfficeTagCompanyIdQuery = '';
	var OfficeTagColourQuery = '';
	try {


		if (OfficeTagType != undefined) { OfficeTagTypeQuery = ',"OfficeTagType": "' + OfficeTagType + '"'; }
		if (OfficeTagName != undefined) { OfficeTagNameQuery = ',"OfficeTagName": "' + OfficeTagName + '"'; }
		if (OfficeTagCompanyId != undefined) { OfficeTagCompanyIdQuery = ',"OfficeTagCompanyId": "' + OfficeTagCompanyId + '"'; }
		if (OfficeTagColour != undefined) { OfficeTagColourQuery = ',"OfficeTagColour": "' + OfficeTagColour + '"'; }

		if (checkExistance != false) {
			if (OfficeTagUserId != undefined) { OfficeTagUserIdQuery = '"OfficeTagModifiedByUserId": "' + OfficeTagUserId + '"'; }
			var OfficeTagUpdatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeTagUpdatedDate": "' + OfficeTagUpdatedDate + '"';


		} else {
			if (OfficeTagUserId != undefined) { OfficeTagUserIdQuery = '"OfficeTagUserId": "' + OfficeTagUserId + '"'; }
			var OfficeTagUpdatedDate = datetime.toISOString();
			var OfficeTagCreatedDate = datetime.toISOString();
			var DateQuery = ',"OfficeTagCreatedDate": "' + OfficeTagCreatedDate + '","OfficeTagUpdatedDate": "' + OfficeTagUpdatedDate + '"';
		}

		var FQuery = OfficeTagUserIdQuery + OfficeTagTypeQuery + OfficeTagNameQuery + OfficeTagCompanyIdQuery + OfficeTagColourQuery + DateQuery;
		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"OfficeTagUserId": "' + OfficeTagUserId + '","OfficeTagType": "' + OfficeTagType + '","OfficeTagName": "' + OfficeTagName + '","OfficeTagCompanyId": "' + OfficeTagCompanyId + '","OfficeTagColour": "' + OfficeTagColour + '"' + DateQuery + '}';
		// console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
