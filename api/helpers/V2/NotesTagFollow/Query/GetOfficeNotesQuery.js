'use strict';
exports.getQueryByOfficeNotes = function (requireQuery, callback) {

	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;

	var userIDValues = requireQuery.query.UserId;
	var officeId= requireQuery.query.OfficeId;

	if ((userIDValues == undefined)) {
		callback({ Error: "Parameter userIDValues is Missing" }, null);
	}
	else {

		try {
			if(officeId == undefined){
				var OfficeNote = '{"bool": {"must":[{"term":{ "OfficeNoteCreatedByUserId" : "' + userIDValues + '"}}] }}';
				var FinalQuery = '{"query": ' + OfficeNote + '}';
				callback(null, FinalQuery);
			}else{
				var OfficeNote = '{"bool": {"must":[{"term":{ "OfficeNoteCreatedByUserId" : "' + userIDValues + '"}},{"term":{ "OfficeNoteOfficeId" : "' + officeId + '"}}] }}';
				var FinalQuery = '{"query": ' + OfficeNote + '}';
				callback(null, FinalQuery);
			}
		} catch (err) {
			callback(err, null);
		}
	}
}

exports.postQueryByOfficeNotes = function (requireQuery, checkExistance, callback) {


	
	var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
	var datetime = new Date();
	var req = JSON.parse(requireQuery.body);
	var OfficeNoteUserId = requireQuery.query.UserId;
	var OfficeNoteText = req.NoteText;
	var OfficeNoteOfficeId = req.OfficeId;
	var OfficeNoteCompanyId = requireQuery.AccountId;

	//var OfficeNoteUserIdQuery = '';
	var OfficeNoteTextQuery = '';
	var OfficeNoteOfficeIdQuery = '';
	var OfficeNoteCompanyIdQuery = '';
	var parent = '';
	try {

		//if (OfficeNoteUserId != undefined) { OfficeNoteUserIdQuery = '"OfficeNoteCreatedByUserId": "' + OfficeNoteUserId + '"'; }
		if (OfficeNoteText != undefined) { OfficeNoteTextQuery = ',"OfficeNoteText": "' + OfficeNoteText + '"'; }
		if (OfficeNoteOfficeId != undefined) { OfficeNoteOfficeIdQuery = ',"OfficeNoteOfficeId": "' + OfficeNoteOfficeId + '"'; }
		if (OfficeNoteCompanyId != undefined) { OfficeNoteCompanyIdQuery = ',"OfficeNoteCompanyId": "' + OfficeNoteCompanyId + '"'; }
		if (parent != undefined) { parent = ',"parent": "' + OfficeNoteOfficeId + '"'; }
		if (checkExistance != false) {
			var OfficeNoteModifiedDateTime = datetime.toISOString();
			var OfficeNoteModifiedByUserId = ',"OfficeNoteModifiedByUserId": "' + OfficeNoteUserId + '"';
			var DateQuery = OfficeNoteModifiedByUserId + ',"OfficeNoteModifiedDateTime": "' + OfficeNoteModifiedDateTime + '"';

		} else {
			//var OfficeNoteModifiedDateTime = datetime.toISOString();


			var OfficeNoteCreatedDateTime = datetime.toISOString();
			var OfficeNoteCreatedByUserId = ',"OfficeNoteCreatedByUserId": "' + OfficeNoteUserId + '"';
			var DateQuery = OfficeNoteCreatedByUserId + ',"OfficeNoteCreatedDateTime": "' + OfficeNoteCreatedDateTime + '"';


		}




		var FQuery = OfficeNoteTextQuery + OfficeNoteOfficeIdQuery + DateQuery + OfficeNoteCompanyIdQuery;

		if (FQuery.substring(0, 1) == ',') {
			var FinalQuery = '{' + FQuery.substring(1) + '}';
		} else {
			var FinalQuery = '{' + FQuery + '}';
		}

		if (checkExistance != false) {
			var FinalQuery = '{ "doc" :' + FinalQuery + '}';

		} else {
			var FinalQuery = FinalQuery;
		}



		//var FinalQuery = '{"OfficeNoteUserId": "' + OfficeNoteUserId + '","OfficeNoteType": "' + OfficeNoteType + '","OfficeNoteName": "' + OfficeNoteName + '","OfficeNoteCompanyId": "' + OfficeNoteCompanyId + '","OfficeNoteColour": "' + OfficeNoteColour + '"' + DateQuery + '}';
		//	console.log("FinalQuery " + FinalQuery);
		callback(null, FinalQuery);
	} catch (err) {
		callback(err, null);
	}
}
