
/********************************************************************************************
This file (GetMetadata.js) is getting data from salesforce which is updated from past hour.

********************************************************************************************/
/*******************************************************************************************/

let SalesforceConnection = require("node-salesforce-connection");
let moment = require('moment');
var jsSalesForceforce = require('jsforce');
var yamlConfig = require('js-yaml');
var fs = require('fs');
var sizeof = require('object-sizeof');
var credentialsObj = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));
let username = credentialsObj.Salesforce.username;
let password = credentialsObj.Salesforce.password;
let token = credentialsObj.Salesforce.token;
let hostname = credentialsObj.Salesforce.host;
let apiVersion = credentialsObj.Salesforce.version;

var exportedRequiredModules = require('../../../../../config.js').requiredModulesExport;
async function handler(query) {

    let sfConn = new SalesforceConnection();


    let queryPath = "/services/data/v" + apiVersion + "/query/?q=";

    await sfConn.soapLogin({
        hostname: hostname,
        apiVersion: apiVersion,
        username: username,
        password: password + token,
    });
    var offset = new Date().getTimezoneOffset();

    //console.log("query " + query);

    let sfInfo = await sfConn.rest(queryPath + encodeURIComponent(query), api = "bulk");
    //  console.log(sfInfo);
    return sfInfo;
}



function salesforceData(query, callback) {

    var initializePromise = handler(query);
    initializePromise.then(function (result) {
        sfDetails = result;
        callback(null, sfDetails);
    }, function (err) {
        callback("Error", null);
    })
}

exports.projectSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.projectQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    // query = query.replace('Type_Id', "'" + searchType + "'")

    //console.log(query);
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                delete SavedSearchRecords.Type__c;
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            SavedSearches.records = SavedSearchResult;

            //   console.log (JSON.stringify(res))
            callback(null, SavedSearches);
        }
    });
}

exports.officeSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.officeQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    // query = query.replace('Type_Id', "'" + searchType + "'")

    //console.log(query);
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                delete SavedSearchRecords.Type__c;
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            SavedSearches.records = SavedSearchResult;

            //   console.log (JSON.stringify(res))
            callback(null, SavedSearches);
        }
    });
}

exports.contactSavedSearchDataList = function (request, callback) {
    var userId = request.query.UserId;
    
    var Size = request.query.Size;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultSavedSearchSize);
    }
    var SavedSearchResult = [];
    var query = credentialsObj.Salesforce.contactQuerySavedSearch.replace('user_Id', "'" + userId + "'");
    // query = query.replace('Type_Id', "'" + searchType + "'")

    //console.log(query);
    salesforceData(query, function (err, SavedSearches) {
        if (err) {
            callback(err, null);
        } else {
            delete SavedSearches.totalSize;
            delete SavedSearches.done;
            SavedSearches.records.forEach(function (SavedSearchRecords, indexNumber) {

                SavedSearchRecords.JSONCriteriaArrayString = JSON.parse(SavedSearchRecords.Field2__c);
                SavedSearchRecords.ElasticSearchQueryString = JSON.parse(SavedSearchRecords.Field3__c);
                delete SavedSearchRecords.Field2__c;
                delete SavedSearchRecords.Field3__c;
                delete SavedSearchRecords.attributes;
                delete SavedSearchRecords.Type__c;
                if (SavedSearchRecords.LastRunDate != null) {
                    SavedSearchRecords.LastRunDate = new Date(SavedSearchRecords.LastRunDate);
                }

            });
            SavedSearches.records.sort(LastUpdateSort);
            SavedSearches.records.forEach(function (SavedSearchFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    SavedSearchResult.push(SavedSearchFinalRecords);

                }

            });
            SavedSearches.records = SavedSearchResult;

            //   console.log (JSON.stringify(res))
            callback(null, SavedSearches);
        }
    });
}

function LastUpdateSort(a, b) {
    return new Date(b.LastRunDate).getTime() - new Date(a.LastRunDate).getTime();
}

exports.searchQuery = function (UserId, SearchId, callback) {

    var query = credentialsObj.Salesforce.queryExecuteSearch.replace('user_Id', "'" + UserId + "'");
    query = query.replace('Search_Id', "'" + SearchId + "'")

    salesforceData(query, function (err, res) {

        if (err) {
            callback(err, null);
        } else {
            if (res.records.length > 0) {
                var salesforceList = res.records[0].Field3__c;
                if (salesforceList != null) {
                    var savedSearchQuery = salesforceList;
                    callback(null, savedSearchQuery);

                }
                else {
                    var SfConnection = new jsSalesForceforce.Connection({ loginUrl: 'https://' + hostname });

                    SfConnection.login(username, password + token, function (err, userInfo) {
                        if (err) {

                            callback(err, null);
                        }
                        else {
                            var elasticQueryName = 'Field3__c.json';
                            //     console.log(elasticQueryName);

                            //    SfConnection.sobject('Attachment').record('00P6E000004zGahUAE').blob('Body').on('data', function(data) {
                            SfConnection.sobject("Attachment ").select('Id').where("ParentId = '" + SearchId + "' AND Name = '" + elasticQueryName + "'").execute(function (error, AttachmentIds) {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    var AttachmentData = SfConnection.sobject('Attachment').record(AttachmentIds[0].Id).blob('Body');
                                    var bufferOutput = [];
                                    AttachmentData.on('data', function (data) {
                                        bufferOutput.push(data);
                                    });


                                    AttachmentData.on('end', function () {
                                        var AttachmentContent = Buffer.concat(bufferOutput);
                                        var savedSearchQuery = AttachmentContent.toString();
                                        // savedSearchQuery = savedSearchQuery.replace(/\n/g, ' ');
                                        // console.log("savedSearchQuery",savedSearchQuery)
                                        callback(null, savedSearchQuery);
                                    });
                                }
                            });
                        }
                    });

                }
            }
            else {
                callback({ "Message": "No Records found" }, null);
            }
        }

    });

}

exports.userProfileDataList = function (request, callback) {

    var userId = request.query.UserId;
    var query = credentialsObj.Salesforce.queryUserProfile.replace('user_Id', "'" + userId + "'");
   
    salesforceData(query, function (error, UserDetails) {
        if (error) {
            callback(error, null);
        } else {
            var accountManagerName = UserDetails.records[0].Contact.Account.Owner_Name__c;
            var accountManagerEmail = UserDetails.records[0].Contact.Account.Owner.Email;
            var accountManagerPhone = UserDetails.records[0].Contact.Account.Owner.Phone;
            delete UserDetails.totalSize;
            delete UserDetails.records[0].Contact;
            delete UserDetails.done;
            delete UserDetails.records[0].attributes;
            UserDetails.records[0]["Account_Manager_Name"] = accountManagerName;
            UserDetails.records[0]["Account_Manager_Email_Address"] = accountManagerEmail;
            UserDetails.records[0]["Account_Manager_Phone_Number"] = accountManagerPhone;
            callback(null, UserDetails);
        }
    });

}

function CreateSavedSearchQuery(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    //var userId = RequestParam.userId;
    //var organisationId = RequestParam.organisationId;
    var organisationId;
    var userId = request.query.UserId;
    var searchName = RequestParam.name;
    var elasticQueryUpdated = {}
    elasticQueryUpdated["filters"] = RequestParam.filters
    elasticQueryUpdated["switches"] = RequestParam.switches
    elasticQueryUpdated["query"] = RequestParam.query
    var elasticQuery = JSON.stringify(elasticQueryUpdated);
    var lastRunCount = RequestParam.lastRunCount;
    var searchType = RequestParam.Type;
    var datetime = new Date();
    var LastRunDateQuery = datetime.toISOString();
    var createdDateQuery = datetime.toISOString();
    var length_elasticQuery = sizeof(elasticQuery) / 1024;
    // let objJsonB64 = Buffer.from(objJsonStr).toString("base64");
    // console.log(length_elasticQuery);
    var elasticQueryName = 'Field3__c.json';
    var query = credentialsObj.Salesforce.queryUserProfile.replace('user_Id', "'" + userId + "'");    
    salesforceData(query, function (error, result) {
            if (error) {
               callback(err, null);
            }
            else {
                    organisationId = result.records[0].AccountId;
                    if (length_elasticQuery < 30) {
                        salesforcefConn.sobject("GP_CustomObject__c").create({ UserId__c: userId, OrganisationId__c: organisationId, Field1__c: searchName,  Field3__c: elasticQuery, Field6__c: lastRunCount, Field4__c: createdDateQuery, Field5__c: LastRunDateQuery, Key_Type_Organisation_User__c: searchType + "_" + organisationId + "_" + userId, Key_Type_Organisation__c: searchType + "_" + organisationId, Type__c: searchType }
                            , function (error, response) {
                                if (error || !response.success) { 
                                    console.log("error",error)
                                    callback({ "Message": "Not Created Successfully" }, null); }
                                else {

                                    callback(null, { "Message": "Created Successfully and SearchId is " + response.id });
                                }
                            }
                        );
                    }
                    else if (length_elasticQuery >= 30) {
                        salesforcefConn.sobject("GP_CustomObject__c").create({ UserId__c: userId, OrganisationId__c: organisationId, Field1__c: searchName, Field6__c: lastRunCount, Field4__c: createdDateQuery, Field5__c: LastRunDateQuery, Key_Type_Organisation_User__c: searchType + "_" + organisationId + "_" + userId, Key_Type_Organisation__c: searchType + "_" + organisationId, Type__c: searchType }
                            , function (error, response) {
                                // console.log(error)
                                if (error || !response.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                else {
                                   let elasticQueryB64 = Buffer.from(elasticQuery).toString("base64");
                                    salesforcefConn.sobject("Attachment").create({ Name: elasticQueryName, Body: elasticQueryB64, ParentId: response.id }
                                        , function (error, responseElasticquery) {
                                            if (error || !responseElasticquery.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                            else {

                                                callback(null, { "Message": "Created Successfully and SearchId: " + response.id + ", ElasticQuery AttachmentID : " + responseElasticquery.id });
                                            }
                                        }
                                    );
                                }
                            }
                        );

                    }
            }
    });
}
function UpdateSavedSearch(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    var SavedSearchId = RequestParam.SavedSearchId;
    if(SavedSearchId == undefined){
        callback({"Error" : "SavedSearchId is missed in updated query"}, null);
    }else{
        var searchName = RequestParam.name;
        var elasticQueryUpdated = {}
        elasticQueryUpdated["filters"] = RequestParam.filters
        elasticQueryUpdated["switches"] = RequestParam.switches
        elasticQueryUpdated["query"] = RequestParam.query
        var elasticQuery = JSON.stringify(elasticQueryUpdated);
        // console.log("elasticQuery",elasticQuery)
        var lastRunCount = RequestParam.lastRunCount;
        var searchType = RequestParam.Type;
        var datetime = new Date();
        var LastRunDateQuery = datetime.toISOString();
        var length_elasticQuery = sizeof(elasticQuery) / 1024;
        var elasticQueryName = 'Field3__c.json';
        // console.log(length_elasticQuery);
        if (length_elasticQuery < 30) {
            salesforcefConn.sobject("GP_CustomObject__c").update({ Id: SavedSearchId, Field1__c: searchName, Field3__c: elasticQuery, Field6__c: lastRunCount, Field5__c: LastRunDateQuery, Type__c: searchType }
                , function (error, response) {

                    // console.log("error",error)
                    if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                    else {

                        callback(null, { "Message": "Updated Successfully and SearchId is " + response.id });
                    }
                }
            );
        }
        else if (length_elasticQuery >= 30) {

            salesforcefConn.sobject("GP_CustomObject__c").update({ Id: SavedSearchId, Field1__c: searchName, Field3__c: '', Field6__c: lastRunCount, Field5__c: LastRunDateQuery, Type__c: searchType }
                , function (error, response) {
                    if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                    else {
                        // console.log("SavedSearchId",SavedSearchId)
                        // console.log("elasticQueryName",elasticQueryName)
                        salesforcefConn.sobject("Attachment ").select('Id').where("ParentId = '" + SavedSearchId + "' AND Name = '" + elasticQueryName + "'").execute(function (error, AttachmentIds) {
                            if (error || !response.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                            else {
                                let elasticQueryB64 = Buffer.from(elasticQuery).toString("base64");
                                // console.log(AttachmentId)
                                if (AttachmentId == undefined){
                                    salesforcefConn.sobject("Attachment").create({ Name: elasticQueryName, Body: elasticQueryB64, ParentId: SavedSearchId }
                                        , function (error, responseElasticquery) {
                                            if (error || !responseElasticquery.success) { callback({ "Message": "Not Created Successfully" }, null); }
                                            else {

                                                callback(null, { "Message": "Created Successfully and SearchId: " + SavedSearchId + ", ElasticQuery AttachmentID : " + responseElasticquery.id });
                                            }
                                        }
                                    );
                                }else{
                                    var AttachmentId = AttachmentIds[0].Id;
                                    // console.log("AttachmentId",AttachmentId)
                                    salesforcefConn.sobject("Attachment").update({ Id: AttachmentId, Name: elasticQueryName, Body: elasticQueryB64 }, function (error, responseElasticquery) {
                                        if (error || !responseElasticquery.success) { callback({ "Message": "Not Updated Successfully" }, null); }
                                        else {
                                            callback(null, { "Message": "Updated Successfully and SearchId: " + response.id + ", ElasticQuery AttachmentID" + responseElasticquery.id });
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            );
        }
    }
}
function DeleteSavedSearch(request, salesforcefConn, callback) {
    var RequestParam = JSON.parse(request.body);
    var SavedSearchId = RequestParam.SavedSearchId;
    salesforcefConn.sobject("Attachment ").select('Id').where("ParentId = '" + SavedSearchId + "'").execute(function (error, AttachmentIds) {

        if (error) { callback({ "Message": "Not Deleted Successfully" }, null); }
        else {

            if (AttachmentIds.length > 0) {
                AttachmentIds.forEach(function (data, index) {
                    var AttachmentId = data.Id
                    salesforcefConn.sobject("Attachment").destroy(AttachmentId, function (error, attachmentResponse) {

                        if (error) { callback({ "Message": "Not Deleted Successfully" }, null); }
                        else {

                            if (index == ((AttachmentIds.length) - 1)) {
                                salesforcefConn.sobject("GP_CustomObject__c").destroy(SavedSearchId, function (error, response) {
                                    if (error || !response.success) { callback({ "Message": "Not Deleted Successfully" }, null); }
                                    else {

                                        callback(null, { "Message": "Deleted Successfully and SearchId is " + response.id });
                                    }
                                }
                                );
                            }
                        }
                    });
                    //   console.log("index",index)  
                })
            }
            else {
                salesforcefConn.sobject("GP_CustomObject__c").destroy(SavedSearchId, function (error, response) {
                    if (error || !response.success) { callback({ "Message": "Not Deleted Successfully" }, null); }
                    else {

                        callback(null, { "Message": "Deleted Successfully and SearchId is " + response.id });
                    }
                }
                );
            }
        }
    });
}
exports.CUDSavedSearch = function (request, opType, callback) {


    var SfConnection = new jsSalesForceforce.Connection({ loginUrl: 'https://' + hostname });

    SfConnection.login(username, password + token, function (err, userInfo) {
        if (err) {

            callback(error, null);
        }

        if (opType == 'Create') {
            CreateSavedSearchQuery(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }
        else if (opType == "Update") {

            UpdateSavedSearch(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }
        else if (opType == "Delete") {


            DeleteSavedSearch(request, SfConnection, function (error, successMessgae) {
                if (error) {
                    callback(error, null);
                }
                else {

                    callback(null, successMessgae)
                }

            });
        }

    });
}


exports.ExportOptionList = function (request, callback) {
    var userId = request.query.UserId;
    var searchType = request.query.TypeId;
//	console.log(request.AccountId);
    var Size = request.query.OptionItemsSize;
    if (Size == undefined) {
        Size = parseInt(credentialsObj.Salesforce.DefaultExportOptionsSize);
    }
    var ExportOptionsResult = [];
    if (searchType != undefined) {
        var query = credentialsObj.Salesforce.queryExportoptions;
        query=query.replace("'CustomFormat_AccountId_UserID' OR Key_Type_Organisation_User__c = 'CustomFormatPdf_AccountId_UserID' OR Key_Type_Organisation__c = 'CustomFormat_Shared_AccountId' OR Key_Type_Organisation__c = 'CustomFormatPdf_Shared_AccountId' OR Type__c = 'CustomFormat_Std' OR Type__c = 'CustomFormatPdf_Std'","'"+searchType+'_'+request.AccountId+'_'+userId+"'");
    }
    else {
        var query = credentialsObj.Salesforce.queryExportoptions;
        query= query.replace(new RegExp('UserID', 'g'), userId);
        query=query.replace(new RegExp('AccountId', 'g'), request.AccountId);
    }
//   console.log(query);
    salesforceData(query, function (err, ExportOptions) {
        if (err) {
            callback(err, null);
        } else {

            delete ExportOptions.totalSize;
            delete ExportOptions.done;
            ExportOptions.records.forEach(function (ExportOptionRecords, indexNumber) {
              ExportOptionRecords.Field2 = JSON.parse(ExportOptionRecords.Field2__c);
                ExportOptionRecords.Field3 = JSON.parse(ExportOptionRecords.Field3__c);
                ExportOptionRecords.Field4 = JSON.parse(ExportOptionRecords.CreatedDate);
                ExportOptionRecords.Field5 = ExportOptionRecords.LastRunDate;
                ExportOptionRecords.Field6 = ExportOptionRecords.LastRunCount;
                ExportOptionRecords.Field7 = ExportOptionRecords.Field7__c;
                ExportOptionRecords.Field8 = ExportOptionRecords.Field8__c;
                ExportOptionRecords.Field9 = ExportOptionRecords.Field9__c;
                ExportOptionRecords.Field10 = ExportOptionRecords.Field10__c;
                ExportOptionRecords.Type = ExportOptionRecords.Type__c;
                delete ExportOptionRecords.attributes;
                delete ExportOptionRecords.LastRunCount;
                delete ExportOptionRecords.LastRunDate;
                delete ExportOptionRecords.CreatedDate;
                delete ExportOptionRecords.Type__c;
                delete ExportOptionRecords.Field2__c;
                delete ExportOptionRecords.Field3__c;
                delete ExportOptionRecords.Field7__c;
                delete ExportOptionRecords.Field8__c;
                delete ExportOptionRecords.Field9__c;
                delete ExportOptionRecords.Field10__c;
                

            });

            ExportOptions.records.forEach(function (ExportOptionFinalRecords, indexNumber) {
                if (indexNumber < Size) {
                    ExportOptionsResult.push(ExportOptionFinalRecords);

                }

                ExportOptions.records = ExportOptionsResult;

                //        console.log (ExportOptions);
            });
            callback(null, ExportOptions);
        }
    });
}