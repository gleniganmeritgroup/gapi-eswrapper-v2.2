'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetUpcomingFollows.js) is used form a query for given UserId.
********************************************************************************************/
/*******************************************************************************************/


exports.getCount = function (requireQuery, callback) {
   var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
   // console.log("beSpokeQuery",requireQuery)
   var userID = requireQuery.query.UserId;    
   if ((userID == undefined)) {
		callback({Error:"Parameter UserId Values is Missing"},null);
	}
	else{
        try{
            var FinalQuery = '{"query":{"bool":{"must":[{"range":{"DateSent":{"gte":"now-2w/d","lte":"now"}}},{"terms":{"UserId":["'+userID+'"]}}]}},"sort":[{"DateSent":{"order":"desc"}}],"aggs":{"by_type":{"terms":{"field":"Type"},"aggs":{"by_day":{"date_histogram":{"field":"DateSent","interval":"day","format":"yyyy-MM-dd"}}}}},"size": 0 }'; 
            //console.log("FinalQuery "+FinalQuery);
            callback(null, FinalQuery);
        }
        catch (err) {
            callback(err, null);
        }         
    }    
}

exports.getFollowAlertTypeDate = function (requireQuery, callback) {

    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

    var userID = requireQuery.query.UserId;
    var DateSelected = requireQuery.query.Date;
    var Type = requireQuery.query.Type;
    var typearr = ["Project","Contact","Office"];

    if ((userID == undefined)) {
        callback({ Error: "Parameter UserId is Missing" }, null);
    }
    else if (DateSelected == undefined) {
        callback({ Error: "Parameter Date is Missing" }, null);
    }
    else if((DateSelected != undefined) && (/^\s*([\d]{2}\-[\d]{2}\-[\d]{4})\s*$/g.exec(DateSelected) == null)){
		callback({Error:"Parameter Date value is invalid. Date format should be \"DD-MM-YYYY\""},null);
	}
    else if (Type == undefined) {
        callback({ Error: "Parameter Type is Missing" }, null);
    }
    else if (Type != undefined && typearr.indexOf(Type) == -1) {
        callback({ Error: "Parameter Type value is invalid." }, null);
    }
    else if ((userID != undefined && DateSelected != undefined && Type != undefined)) {        
        try {
            var FromDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT00:00:00");
            var ToDateTime = exportedRequiredModules.moment(DateSelected, 'DD-MM-YYYY').format("YYYY-MM-DDT23:59:59");   
            var FinalQuery = '{"query":{ "bool":{"must":[{"range":{"DateSent": {"gte":"'+FromDateTime+'", "lte": "'+ToDateTime+'"}}},{"terms":{"UserId":["'+userID+'"]}},{"terms":{ "Type" : ["'+Type+'"]}}]}}}';
            //console.log("FinalQuery "+FinalQuery);
            callback(null, FinalQuery);
        } catch (err) {
            callback(err, null);
        }        
    }
}