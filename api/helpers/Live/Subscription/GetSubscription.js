'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetSubscriptionFromMongoDB.js) is used get a subscription Json from MongoDB.
********************************************************************************************/
/*******************************************************************************************/
var yamlConfig = require('js-yaml');
var fs = require('fs');
let SalesforceConnection = require("node-salesforce-connection");
let moment = require('moment');

/******************************************************************************
Function: getSubscriptionJSON
Argument:keyValue
Return: return json 
Usage:
	1. Returns a Subscription JSON from Mongodb.
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/


async function handler (userID)  {
    
    let sfConn = new SalesforceConnection();
    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    
    var credentialsObj = yamlConfig.safeLoad(fs.readFileSync('./config/Credentials.yml', 'utf8'));
    
    let username = credentialsObj.Salesforce.username;
    let password = credentialsObj.Salesforce.password;
    let token = credentialsObj.Salesforce.token;
    let hostname = credentialsObj.Salesforce.host;
    let apiVersion = credentialsObj.Salesforce.version;

    let queryPath = "/services/data/v"+apiVersion+"/query/?q=";

    await sfConn.soapLogin({
    hostname: hostname,
    apiVersion: apiVersion,
    username: username,
    password: password+token,
    });
    
    var query = credentialsObj.Salesforce.query.replace('userID', "'"+userID+"'");
    
    let recentAccounts = await sfConn.rest(queryPath + encodeURIComponent(query));
    return recentAccounts 
}

function subscription(userID, callback){
    //console.log(userID);
    var initializePromise =  handler(userID);

    initializePromise.then(function(result) {
        var userDetails = result;
      //  console.log(userDetails);
        callback(null,userDetails);
    }, function(err) {
        callback(err,null);
    })
}

// function getSubscriptionJSON (userID, callback) {
exports.getSubscriptionJSON= function (userID, callback) {
    subscription(userID, function(error,response){
        if(error){
            callback(error,null);
        }else{
        var subscriptionList = []
        for (let account of response.records) {
            // console.log("Account " + account.Name + " was created recently.");
            var subscriptionJSON = {}
            for(var idx in account) {
                subscriptionJSON[idx] = account[idx];
            }
            // if(pageCheck == 0 ){ subscriptionJSON["PageLimit"] = 100; }
            // subscriptionJSON["IsActive"] = "Y";
            // subscriptionJSON["ReplicationDateTime"] = moment().format("YYYY-MM-DDTHH:mm:ss")+'.000+0000';
            subscriptionList.push(subscriptionJSON);
        }   
        
        callback(null,subscriptionList);
        }
    });
    
}
// var userID='0056E000002Dplh'
// getSubscriptionJSON(userID, function (error, subscriptionList) {
//     console.log("subscriptionList",subscriptionList[0].Id)

// })