'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetContractDevTypeQuery.js) is used form a query for given development types and contract types .
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getQueryByContractDevType
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query
Usage:
	1. Forms a elastic query for searching contract type and development type. 
	2. 'getSubscriptionQueryObj' returns location and sector based on subscription json.
	3. 'getContractDevTypeMetadata' returns metadata of contract type and development type.
	4. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByContractDevType = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var contractTypesValues = requireQuery.query.ContractTypes;
	var developmentTypesValues = requireQuery.query.DevelopmentTypes;
	
	var contractTypesValuesList = [];
	var developmentTypesValuesList = [];
	if ((contractTypesValues == undefined) || (developmentTypesValues == undefined)){
		callback( {Error:"Need to give both parameters ContractTypes and DevelopmentTypes"}, null);
	}else{
		if (contractTypesValues != undefined){
			contractTypesValues.split(',').forEach(function (contractType){
			if (contractType.toLowerCase() != 'contract types'){
				contractTypesValuesList.push(contractType.toLowerCase());
			}
		});}

		if (developmentTypesValues != undefined) {
			developmentTypesValues.split(',').forEach(function (developmentType){
			if (developmentType.toLowerCase() != 'development types'){
				developmentTypesValuesList.push(developmentType.toLowerCase());
			}
		});}
		
		exportedRequiredModules.getSubscriptionQueryObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}
			else{
				try{	
					getContractDevTypeMetadata(exportedRequiredModules, contractTypesValuesList, developmentTypesValuesList, function(err, contractTypeDataList, developmentTypeDataList){
					if (err){
						callback(err, null);
					}
					else if ((developmentTypeDataList.length != developmentTypesValuesList.length) && (developmentTypesValuesList.length > 0)) {
						callback({Error: 'Invalid DevelopmentTypes value is given.'}, null);
					}
					else if((contractTypeDataList.length != contractTypesValuesList.length) && (contractTypesValuesList.length > 0)){
						callback({Error: 'Invalid ContractTypes value is given.'}, null);
					}
					else{
					
						var queryBuildDevelopmentType = {};
						var queryBuildContractType = {};
						
						var queryFieldDevelopmentType = '{"terms": %s}';
						var queryFieldContractType = '{"terms": %s}';
						
						queryBuildDevelopmentType.DevelopmentType = developmentTypeDataList;
						queryBuildContractType.ContractType = contractTypeDataList;
						
						var developmentQueryFormation, contractQueryFormation;
					
						if (developmentTypeDataList.length > 0 ){ developmentQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldDevelopmentType, JSON.stringify(queryBuildDevelopmentType))+' ]}}'; }
						if (contractTypeDataList.length > 0 ){ contractQueryFormation = '{"bool": {"should": [ '+exportedRequiredModules.convertComma2Array(queryFieldContractType, JSON.stringify(queryBuildContractType))+' ]}}'; }
						
						try{
							var projectHistories =exportedRequiredModules.getTimeRangeQueryObj.byTimeRange(exportedRequiredModules,requireQuery);
							var sortBy = exportedRequiredModules.getSortbyQueryObj.SortByCondition(exportedRequiredModules,requireQuery);
							
							var queryConcat=projectHistories
							if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
							if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
							if (contractQueryFormation != ''){ queryConcat = queryConcat +', '+contractQueryFormation; }
							if (developmentQueryFormation != '' ){ queryConcat = queryConcat +', '+developmentQueryFormation; }
							var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
							// console.log("FinalQuery"+FinalQuery);
							callback(null, FinalQuery);
						}catch (err){
							callback(err, null);
						}
					}
				});
				}catch (err){
					callback(err, null);
				}
			}
		});
	}
}
/******************************************************************************
Function: getContractDevTypeMetadata
Argument:exportedRequiredModules, contractTypesValuesList, developmentTypesValuesList
Return: get contract type and development type.
Usage:
	1. Extracts contract type and development type from metadata
*******************************************************************************/
function getContractDevTypeMetadata(exportedRequiredModules, contractTypesValuesList, developmentTypesValuesList, callback){
	
	var developmentTypeDataList = [];
	var contractTypeDataList = [];

	try{
	 	var urlRequestDevelopmentType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectdevelopmenttypes';
		 var urlRequestContractType = exportedRequiredModules.elasticIndexHost+"/"+exportedRequiredModules.elasticIndexName+'/metadata/projectcontracttypes';

		exportedRequiredModules.request(urlRequestDevelopmentType, function (error, response,developmentTypeContent) {
			if( developmentTypeContent != undefined){
				var developmentTypeData = JSON.parse(developmentTypeContent);
				developmentTypeData._source.ProjectDevelopmentTypes.forEach(function(developmentType) {
					if (developmentTypesValuesList.length > 0)  {
						if (developmentTypesValuesList.includes(developmentType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
							developmentTypeDataList.push(developmentType);
						}
					}
					else{
						developmentTypeDataList.push(developmentType);
					}
				});
				//
				exportedRequiredModules.request(urlRequestContractType, function (error, response,contractTypeContent) {
					if( contractTypeContent != undefined){
						var contractTypeData = JSON.parse(contractTypeContent);
						contractTypeData._source.ProjectContractTypes.forEach(function(contractType) {
							if (contractTypesValuesList.length > 0) {
								if (contractTypesValuesList.includes(contractType.toLowerCase().replace(', ',' and ').replace('&','and')) == true){
									contractTypeDataList.push(contractType);
								}
							}
							else{
								contractTypeDataList.push(contractType);
							}
						});
						
						callback(null, contractTypeDataList, developmentTypeDataList);
					}
					else{
						callback({Error: 'Contract Type metadata URL response failed.'}, null);
					}
				});
			}
			else{
				callback({Error: 'Development Type metadata URL response failed.'}, null);
			}
		});
	}catch(err){
		throw({Error: 'Development Type metadata URL response failed.'});
	}
}