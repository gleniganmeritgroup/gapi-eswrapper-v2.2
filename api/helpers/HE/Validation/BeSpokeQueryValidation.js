'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (BeSpokeQueryValidation.js) is used to validate the Bespoke query
********************************************************************************************/
/*******************************************************************************************/

/******************************************************************************
Function: getUniqueValues
Argument: Array
Return: Array
Usage:
    1. To get unique values from an array values
*******************************************************************************/
function getUniqueValues(arrayValues) {
    var uniqueValues = arrayValues.filter(function(elem, index, self) { 
        return index === self.indexOf(elem);
    });
    return uniqueValues;
}
/******************************************************************************
Function: replaceNonFilterOption
Argument: query,type1,type2,type3
Return: query response
Usage:
    1. Replace braces,comma for query with no filter.
*******************************************************************************/
function replaceNonFilterOption(query,type1,type2,type3){
    query = query.replace(type1, ',');
    query = query.replace(type2, '{');
    query = query.replace(type3, '}');
    return query;
}
/******************************************************************************
Function: replaceForJsonFormat
Argument: query,type1,type2,type3
Return: query response
Usage:
    1. Replace square brackets for query with filter.
*******************************************************************************/
function replaceForJsonFormat(query,type1,type2,type3){
    query = query.replace(type1, '');
    query = query.replace(type2, ']');
    query = query.replace(type3, '[]');
    return query;
}
/******************************************************************************
Function: replaceEmptyBool
Argument: beSpokeQuery
Return: query response
Usage:
    1. Replace empty bool in query if filter is present.
*******************************************************************************/
function replaceEmptyBool(beSpokeQuery){

    var regexForRemoveEmptyListType1 = new RegExp('\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\,','ig');
    var regexForRemoveEmptyListType2 = new RegExp('\\,\\s*\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\]','ig');
    var regexForRemoveEmptyListType3 = new RegExp('\\[\\s*\\{\\"bool\\"\\s*\\:\\s*\\{\\"should\\"\\s*\\:\\s*\\[\\s*\\]\\}\\}\\s*\\]','ig');
    beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    return beSpokeQuery;
}
/******************************************************************************
Function: replaceEmptyBoolForNonFilterOption
Argument: beSpokeQuery
Return: query response
Usage:
    1. Replace empty bool in query if no filter is present.
*******************************************************************************/
function replaceEmptyBoolForNonFilterOption(beSpokeQuery){

    var regexForRemoveEmptyListType1 = new RegExp('\\,\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\,','ig');
    var regexForRemoveEmptyListType2 = new RegExp('\\{\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\,','ig');
    var regexForRemoveEmptyListType3 = new RegExp('\\,\\s*\\"(?:should|must)\\"\\s*\\:\\s*\\[\\s*\\]\\s*\\}','ig');
    beSpokeQuery = replaceNonFilterOption(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    return beSpokeQuery;
}
/******************************************************************************
Function: replaceUnauthorizedLocationValues
Argument: unauthorizedValues, beSpokeQuery
Return: query response
Usage:
    1. Replace unauthorized location in query.
*******************************************************************************/
function replaceUnauthorizedLocationValues(unauthorizedValues, beSpokeQuery){
  
    unauthorizedValues.forEach(function(values){
        var regexForRemoveValuesType1 = new RegExp('\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\,','ig');
        var regexForRemoveValuesType2 = new RegExp('\\,\\s*\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\]','ig');
        var regexForRemoveValuesType3 = new RegExp('\\[\\s*\\{\\s*\\"term\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\}\\s*\\}\\s*\\]','ig');
        var regexForRemoveValuesInListType1 = new RegExp('\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\,','ig');
        var regexForRemoveValuesInListType2 = new RegExp('\\,\\s*\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\]','ig');
        var regexForRemoveValuesInListType3 = new RegExp('\\[\\s*\\"[^\\"]*?'+values+'[^\\"]*?\\"\\s*\\]','ig');
        var regexForRemoveEmptyListType1 = new RegExp('\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*}\\s*\\}\\,','ig');
        var regexForRemoveEmptyListType2 = new RegExp('\\,\\s*\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*\\}\\s*}\\s*\\]','ig');
        var regexForRemoveEmptyListType3 = new RegExp('\\[\\s*\\{\\s*\\"terms\\"\\:\\s*\\{\\s*\\"LocationLevel[\\d]+Facet\\"\\:\\s*\\[\\]\\s*\\}\\s*}\\s*\\]','ig');
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveValuesType1,regexForRemoveValuesType2,regexForRemoveValuesType3);
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveValuesInListType1,regexForRemoveValuesInListType2,regexForRemoveValuesInListType3);
        beSpokeQuery = replaceForJsonFormat(beSpokeQuery,regexForRemoveEmptyListType1,regexForRemoveEmptyListType2,regexForRemoveEmptyListType3);
    });
    
    return beSpokeQuery;
}
/******************************************************************************
Function: queryValidation
Argument:subscriptionJSON, requireQuery,fieldsList, limits
Return: return a elastic search query 
Usage:
	1. Forms a elastic query based on request body and subscription data. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.queryValidation = function (subscriptionJSON, requireQuery,fieldsList, limits, callback){
    var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
    var beSpokeQuery='';
    
    try{
        beSpokeQuery = JSON.parse(requireQuery.body);
    }catch(err){ 
        beSpokeQuery= ''; 
    }
    if (((beSpokeQuery == '') && (requireQuery.body != ''))  || ((beSpokeQuery.query == undefined) && (requireQuery.body != ''))){
        callback({Error: "Invalid Bespoke query"}, null);
    }
    else if(requireQuery.body == ''){
        // callback({Error: "Invalid Bespoke query given."}, null);
        exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
            var queryConcat = '';
            var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
            if ((locationQuery != '') && (locationQuery != null)){ queryConcat = locationQuery; }
            if ((sectorQuery != '') && (sectorQuery != null)){ queryConcat = queryConcat +', '+sectorQuery; }
            if(queryConcat != ''){
                var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
                callback(null, FinalQuery);
            }
            else{
                callback(null, {"took": 1,"timed_out": false,"_shards": {"total": 0,"successful": 0,"failed": 0},"hits": {"total": 0,"max_score": null,"hits": []}});    
            }
            
        });
        
    }else{
        if(beSpokeQuery.from == undefined){
            beSpokeQuery.from = limits[0];
        }
        if(beSpokeQuery.size == undefined){
            beSpokeQuery.size = limits[1];
        }
        
        beSpokeQuery = JSON.stringify(beSpokeQuery);
        var jsonObject = JSON.parse(subscriptionJSON);
        
        // console.log("beSpokeQuery"+beSpokeQuery);
        var locationRegex = /\"LocationLevel[\d]+Facet\"\s*\:\s*(?:\"([^\"]*?)\"|\[([^\]]*?)\])/ig;
        var subscribedLocationQuery;
        var locationLevel1 = [];
        var locationLevel2 = [];
        var locationLevel3 = [];
        var matchesLevel1;
        
        while (matchesLevel1 = locationRegex.exec(beSpokeQuery)) {
            
            if (matchesLevel1[1] != undefined){
                var locationsGroup = matchesLevel1[1].split("#");
                if (locationsGroup[0] != undefined){locationLevel1.push(locationsGroup[0]);}
                if (locationsGroup[1] != undefined){locationLevel2.push(locationsGroup[1]);}
                if (locationsGroup[2] != undefined){locationLevel3.push(locationsGroup[2]);}
            }
            else if (matchesLevel1[2] != undefined){
                var modifiedList = matchesLevel1[2].replace(/\"\s*\,\s*\"/g,'"ListSplit"');
                modifiedList.split('ListSplit').forEach(function(result){
                    var locationsGroup =result.split("#");
                    if (locationsGroup[0] != undefined){locationLevel1.push(locationsGroup[0].replace(/\"/g,''));}
                    if (locationsGroup[1] != undefined){locationLevel2.push(locationsGroup[1].replace(/\"/g,''));}
                    if (locationsGroup[2] != undefined){locationLevel3.push(locationsGroup[2].replace(/\"/g,''));}
                });
            }

        }
        
        getSubscriptionData(subscriptionJSON, function(err, subscribedLocation, subscribedSector){
            if(err){
                callback(err, null); 
            }
            else{
                subscribedLocationQuery = '{"bool": {"should":['+subscribedLocation+']}}';;
                locationLevel1 = getUniqueValues(locationLevel1);
                locationLevel2 = getUniqueValues(locationLevel2);
                locationLevel3 = getUniqueValues(locationLevel3);
                exportedRequiredModules.getMetadataHEObj.getLocationsTownsPostcodes(jsonObject,"search", function(err, authorizedRegions,authorizedCounties,authorizedTowns,authorizedPostcodes){
                
                    if(err){
                        callback(err, null);
                    }
                    else{
                        var notSubscribedRegions = [];
                        var notSubscribedCounties = [];
                        var notSubscribedTowns = [];
                        
                        if(locationLevel1 != ''){notSubscribedRegions = exportedRequiredModules.arrayDiff(locationLevel1, authorizedRegions);}
                        if(locationLevel2 != ''){ notSubscribedCounties = exportedRequiredModules.arrayDiff(locationLevel2, authorizedCounties);}
                        if(locationLevel3 != ''){ notSubscribedTowns = exportedRequiredModules.arrayDiff(locationLevel3, authorizedTowns);}
                        var updatedBeSpokeQuery='';
                        updatedBeSpokeQuery = replaceUnauthorizedLocationValues(notSubscribedRegions, beSpokeQuery);
                        updatedBeSpokeQuery = replaceUnauthorizedLocationValues(notSubscribedCounties, updatedBeSpokeQuery);
                        updatedBeSpokeQuery = replaceUnauthorizedLocationValues(notSubscribedTowns, updatedBeSpokeQuery);
                        if(beSpokeQuery.includes('"filter"') == true){
                            updatedBeSpokeQuery = replaceEmptyBool(updatedBeSpokeQuery);
                        }
                        else{
                            updatedBeSpokeQuery = replaceEmptyBoolForNonFilterOption(updatedBeSpokeQuery);
                        }                        
                        var updatedBeSpokeQueryTemp = updatedBeSpokeQuery
                        
                        updatedBeSpokeQueryTemp = updatedBeSpokeQueryTemp.replace(/\"_source\"\s*\:\[[^>]*?]/ig, '');
                        
                        if ((locationLevel1.length > 0) && (updatedBeSpokeQueryTemp.includes('LocationLevel') == false)){
                            callback(null, {"took": 1,"timed_out": false,"_shards": {"total": 0,"successful": 0,"failed": 0},"hits": {"total": 0,"max_score": null,"hits": []}});    
                        }
                        else {
                            
                            if ((locationLevel1.length == 0) && (updatedBeSpokeQueryTemp.includes('ProjectLocationLevel') == false)){
                                
                                if(updatedBeSpokeQuery.includes('"filter"') == true){
                                    updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"bool":{"filter":[{', '{"bool":{"filter":['+subscribedLocationQuery+',{');
                                }else if(updatedBeSpokeQuery.includes('"should":[') == true){
                                    updatedBeSpokeQuery = updatedBeSpokeQuery.replace('"should":[', '"should":['+subscribedLocation+',');
                                }else if(updatedBeSpokeQuery.includes('{"query":{"bool":{"must":[') == true){
                                    updatedBeSpokeQuery = updatedBeSpokeQuery.replace('{"query":{"bool":{"must":[', '{"query":{"bool":{"should":['+subscribedLocation+'],"must":[');
                                }
                            }
                            callback(null, updatedBeSpokeQuery);
                        }
                    }
                });
            }
        });
    }
}
/******************************************************************************
Function: getSubscriptionData
Argument:subscription
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for given subscribed location and sector. 
*******************************************************************************/
function getSubscriptionData (subscription, callback) {
	try{
        var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;
        var object = JSON.parse(subscription);
        
        var locationLevel1 = [];
        var locationLevel2 = [];
        var sectorData = [];
        
        for (let level1 of object.Subscription.ProjectRegions) {
            var region =  level1.Region;
            var county =  level1.Counties;
            if (county != ""){
                exportedRequiredModules._.each(county, function ( instance){
                    var concatRegion = region+"#"+instance
                    locationLevel2.push(concatRegion.toLowerCase());
                });
            }
            else{
                locationLevel1.push(region.toLowerCase());
            }
        }
        
        for (let sectorLevel of object.Subscription.ProjectSectors) {
            var sector =  sectorLevel.Sector;
            if (sector != ""){
                
                sectorData.push(sector.toLowerCase());
            } 
        }
        
        var queryBuildLevel1={};
        var queryBuildLevel2={};
        var queryBuildSector={};

        queryBuildLevel1.LocationLevel1Facet=locationLevel1;
        queryBuildLevel2.LocationLevel2Facet=locationLevel2;
        queryBuildSector.SectorsGroupString=sectorData;

        var queryFieldLevel1='{"terms": %s}';
        var queryFieldLevel2='{"terms": %s}';
        var queryFieldSector='{"terms": %s}';

        var ProjectLocationLevel1Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel1, JSON.stringify(queryBuildLevel1)); 
        var ProjectLocationLevel2Facet = exportedRequiredModules.convertComma2Array(queryFieldLevel2, JSON.stringify(queryBuildLevel2)); 
        var PrimaryAndSecondarySectorParents = exportedRequiredModules.convertComma2Array(queryFieldSector, JSON.stringify(queryBuildSector)); 
        var locationQuery,sectorQuery;
        if (locationLevel2.length > 0){
            locationQuery = ProjectLocationLevel1Facet+','+ProjectLocationLevel2Facet;
        }
        else if (locationLevel1.length > 0){
            locationQuery = ProjectLocationLevel1Facet;
        }
        if (sectorData.length > 0){
            sectorQuery = PrimaryAndSecondarySectorParents;
        }	
        
        callback(null,locationQuery,sectorQuery);
    }
    catch(err){
        callback(err,null, null);
    }
}
