'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*******************************************************************************************/
/********************************************************************************************
This file (GetValuesQuery.js) is used form a query for given Values .
********************************************************************************************/
/*******************************************************************************************/ 

/******************************************************************************
Function: getQueryByValues
Argument:subscriptionJSON, requireQuery
Return: return a elastic search query 
Usage:
	1. Forms a elastic query for Values. 
	2. This Function is exported as it will be visible to rest of program when
	   called from controller function.
*******************************************************************************/
exports.getQueryByValues = function (subscriptionJSON, requireQuery, callback) {
	
	var exportedRequiredModules = require('../../../../config.js').requiredModulesExport;

	var jsonObject = JSON.parse(subscriptionJSON);
	var fromValues = requireQuery.query.From;
	var toValues = requireQuery.query.To;
	
	if ((fromValues == undefined) && (toValues == undefined)) {
		callback({Error:"Parameter From/To Values is Missing"},null);
	}
	else{
	
		exportedRequiredModules.getSubscriptionQueryHEObj.getQuery(subscriptionJSON, function(err, locationQuery,sectorQuery){
			if(err){
				callback(err, null);
			}else{	
				convertValuesIntoMillions(exportedRequiredModules, fromValues, toValues, function(err, convertedFromValues, convertedToValues)
				{
					
						if (err){
							callback(err,null);
						}
						else{
							if ((convertedFromValues != "") && (convertedToValues != "") && (convertedFromValues > convertedToValues))
							{
								callback({Error : "Your upper limit must be a higher value than your lower limit."}),null;
							}
							else{
								try{
								var valuesQuery='';
								if ((convertedFromValues != "") && (convertedToValues != "")){
									valuesQuery = '{ "range": { "Value": { "gte": "'+convertedFromValues+'", "lte": "'+convertedToValues+'" } } }'
								}
								else if ((convertedFromValues != "") && (convertedToValues == "")){	
									valuesQuery = '{ "range": { "Value": { "gte": "'+convertedFromValues+'" } } }'
								}
								else if ((convertedFromValues == "") && (convertedToValues != "")){	
									valuesQuery = '{ "range": { "Value": { "lte": "'+convertedToValues+'" } } }'
								}
								
								var projectHistories =exportedRequiredModules.getTimeRangeQueryHEObj.byTimeRange(exportedRequiredModules,requireQuery);
								var sortBy = exportedRequiredModules.getSortbyQueryHEObj.SortByCondition(exportedRequiredModules,requireQuery);
								
								var queryConcat=projectHistories
								if (locationQuery != ''){ queryConcat = queryConcat +', '+locationQuery; }
								if (sectorQuery != ''){ queryConcat = queryConcat +', '+sectorQuery; }
								
								if (valuesQuery != '' ){ queryConcat = queryConcat +', '+valuesQuery; }
								var FinalQuery='{"query": {"bool": {"filter": ['+queryConcat+']}}, '+sortBy+'}';
								// console.log("FinalQuery"+FinalQuery);
								callback(null, FinalQuery);
							}catch (err){
								callback(err, null);
							}
							}
						}
					
				});
			}
		});
	}
}
/******************************************************************************
Function: convertValuesIntoMillions
Argument:exportedRequiredModules, fromValues, toValues
Return: from value and two value in millions. 
Usage:
	1. Returns converted from and to values in millions. 
*******************************************************************************/
function convertValuesIntoMillions(exportedRequiredModules, fromValues, toValues, callback){
	
		var convertedFromValues = valuesConversion(fromValues);
		var convertedToValues = valuesConversion(toValues);
		if (convertedFromValues.hasOwnProperty('Error') == true )
		{
			callback(convertedFromValues, null);
		}
		else if (convertedToValues.hasOwnProperty('Error') == true )
		{
			callback(convertedToValues, null);
		}
		else{
			callback(null, convertedFromValues, convertedToValues);
		}
}
/******************************************************************************
Function: valuesConversion
Argument:inputValues
Return: converted value
Usage:
	1. converts value to millions
*******************************************************************************/
function valuesConversion(inputValues){
	try{
		if (inputValues != undefined){
			var valuesRegex = /^([\d\.]+)([\w])?$/g;
			var daysCountPattern = valuesRegex.exec(inputValues);
			if (daysCountPattern != null){
				if (parseFloat(daysCountPattern[1]) == daysCountPattern[1] )
				{
					var valueType;
					var value = parseFloat(daysCountPattern[1]);
					if (daysCountPattern[2] != undefined){
						valueType = daysCountPattern[2];
						if (valueType.toLowerCase() == "k"){ value = (value*1000)/1000000;}
						else if (valueType.toLowerCase() == "b"){ value = (value*1000000000)/1000000; }
						else if (valueType.toLowerCase() != "m"){ 
							return {Error : "Invalid Value format"};
						}
					}
					
					return value;
				}
				else{
					return {Error : "Invalid value"};
				}
			}
			else{
				return {Error : "Invalid Value format"};
				
			}
		}
		else{
			return "";
			
		}
	}catch(err){
		return {Error : "Exception on Value data"};
	}
}